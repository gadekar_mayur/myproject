package billingsystem;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    static Connection con = null;
	public static Connection getDBConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
                     //   System.out.println("Driver Registered");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/billing","root","root");
                     //   System.out.println("Connection generated");
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
    
}
