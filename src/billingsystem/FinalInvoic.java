
package billingsystem;

import static billingsystem.DBConnection.con;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import static org.apache.poi.hssf.usermodel.HeaderFooter.date;

public class FinalInvoic extends javax.swing.JFrame {

    float num;
    String s;
    private Connection con;
    private int cid;
    int bill_number;
     String string;
      private double total;
    private int id3;
    Statement statement,st;
    ResultSet rs;
    String item_Name;
    int qty,total_Amt,price,id1;
    private String time1;
    private String time2;
    String name;
    String add;
    int phone;
    String date;      
    double totbill;
    PreparedStatement ps1,pst;
    // String s;
    public FinalInvoic() {
        setUndecorated(true);
      initComponents();
      DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
        date=dateformat.format(new Date());
        dt1.setText(date);
        
      initData();
     
       initshow();
        initlogo();
    }

     public void initlogo(){
        try{
        ps1 = (PreparedStatement) con.prepareStatement("select logo from shop_details ORDER BY sid DESC LIMIT 1");
          
           rs=ps1.executeQuery();
             
             if (rs.next())
             {
               byte[] im= rs.getBytes("logo");
                  // byte[] imageBytes=rs.getBytes(1);
                  Image image=getToolkit().createImage(im);
                  ImageIcon icon=new ImageIcon(image);
                    label.setIcon(icon); 
             }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public void initshow(){
        try{
         con=DBConnection.getDBConnection();
          Statement st=con.createStatement();
          String sql="select * from shop_details ORDER BY sid DESC LIMIT 1";
          ResultSet rs =st.executeQuery(sql);
          if(rs.next()){
               System.out.println(rs.getString(2));
                shopname.setText(rs.getString(2));
                    
                    mo_no.setText(rs.getString(4));
                    address.setText(rs.getString(3));
                     Statement st3=con.createStatement();
       ResultSet rst1=st3.executeQuery("SELECT invoiceNo FROM invoice ORDER BY invoiceNo DESC LIMIT 1");
               if(rst1.next()){
               
                      id3=rst1.getInt(4)+1;
                                }
               else
               {
                 id3=1;
               }
               bill_no1.setText(String.valueOf(id3));
          }
        }catch(Exception e){}      
               
    }
 public FinalInvoic(int cid, int bill_no) {
        initComponents();
        this.pack();
        this.setLocationRelativeTo(null);        
        bill_number=bill_no;
        this.cid=cid;
        System.out.println("In invoice cid---"+cid);
        System.out.println("In invoice bill no---"+bill_number+"  "+bill_no);
       
       try{
        con = DBConnection.getDBConnection();
        statement=con.createStatement();
        rs=statement.executeQuery("select * from sales where bno ='"+bill_number+"'");
        while(rs.next())
       {
           System.out.println("In create Invoice  code ");
           item_Name=rs.getString("item_name").toString();
           price=rs.getInt("price");
           qty=rs.getInt("qty");
           total_Amt=rs.getInt("total");
           
           System.out.println("item name="+item_Name+" qty="+qty+" total_Amt="+total_Amt+" price="+price);
           st=con.createStatement();
         //  st.executeUpdate("insert into invoice(bill_no,cid,item_name,qty,price,total) values('"+bill_number+"','"+cid+"','"+item_Name+"','"+qty+"','"+price+"','"+total_Amt+"')");
           st.executeUpdate("insert into sales");
       }
       System.out.println("OUT create Invoice  code ");
        }
        catch(Exception e){
        e.printStackTrace();
        }
    }
     
public void initData(){
        
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
            
            try{ 
            con = DBConnection.getDBConnection();
             Statement st=con.createStatement();
                   ResultSet rst1=st.executeQuery("SELECT bno FROM sales ORDER BY bno DESC LIMIT 1");
               if(rst1.next()){
               
                      id1=rst1.getInt(1);
                                }
               else
               {
                 id1=1;
               }
             //  bill_no1.setText(String.valueOf(id1));
               ino.setText(String.valueOf(id1));
       System.out.println("Invoice no= "+id1);
       Statement st2=con.createStatement();
       
           ResultSet rs2=st2.executeQuery("select item_name,qty,price,total from sales where bno='"+id1+"'");
         
            while(rs2.next())
            {
                total=total+Double.parseDouble(rs2.getString(4));
                model.addRow(new Object[]{rs2.getString(1),rs2.getString(2),rs2.getString(3),rs2.getString(4)});
                
            }tot.setText(string.valueOf(total));
       }catch(Exception e){
                System.out.println("Inside Show All product details");
                e.printStackTrace();
        }
    }
        @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        bill_no = new javax.swing.JLabel();
        bn = new javax.swing.JLabel();
        less_room = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        rn = new javax.swing.JLabel();
        dt = new javax.swing.JLabel();
        gn = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        pax = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        arr_dt = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        dep_dt = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        arr_tm = new javax.swing.JLabel();
        dep_tm = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        cmp = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        days1 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        copy = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        in_word = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        checkout_name = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        paid_out = new javax.swing.JLabel();
        net_bill1 = new javax.swing.JLabel();
        adv = new javax.swing.JLabel();
        bill_amt = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        d = new javax.swing.JLabel();
        dis = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel38 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        bill_no1 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        dt1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        itable = new javax.swing.JTable();
        jLabel46 = new javax.swing.JLabel();
        ino = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        shopname = new javax.swing.JLabel();
        mo_no = new javax.swing.JLabel();
        address = new javax.swing.JLabel();
        label = new javax.swing.JLabel();
        tot = new javax.swing.JLabel();

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel2.setText("Bill No.:");
        jPanel6.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel3.setText("Date :");
        jPanel6.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 140, 45, 10));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel4.setText("Room No. :");
        jPanel6.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 160, -1, -1));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel5.setText("Guest Name :");
        jPanel6.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, 20));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel6.setText("Reg. No. :");
        jPanel6.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, -1));

        bill_no.setText("bill");
        jPanel6.add(bill_no, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 100, 14));

        bn.setText("book");
        jPanel6.add(bn, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 100, 14));

        less_room.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(less_room, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 510, 80, 20));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Tariff", "EP", "LT", "ST", "SC", "SBC", "KKC", "F & B", "Laundry Bill", "Other", "Total"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        jPanel6.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 550, 230));

        rn.setText("jLabel19");
        jPanel6.add(rn, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 160, 110, -1));

        dt.setText("jLabel19");
        jPanel6.add(dt, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 136, 110, -1));

        gn.setText("gname");
        jPanel6.add(gn, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 180, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("No Of Pax :");
        jPanel6.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 180, 60, 20));

        pax.setText("jLabel9");
        jPanel6.add(pax, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 180, 100, 20));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Arrival Date :");
        jPanel6.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 80, 20));

        arr_dt.setText("jLabel17");
        jPanel6.add(arr_dt, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 200, -1, 20));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Departure Date :");
        jPanel6.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, -1, -1));

        dep_dt.setText("jLabel19");
        jPanel6.add(dep_dt, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, -1, -1));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Arrival Time :");
        jPanel6.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 200, 80, 20));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("Departure Time :");
        jPanel6.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 220, -1, -1));

        arr_tm.setText("jLabel17");
        jPanel6.add(arr_tm, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 200, -1, 20));

        dep_tm.setText("jLabel19");
        jPanel6.add(dep_tm, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 220, -1, -1));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Company Name :");
        jPanel6.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 240, 100, -1));

        cmp.setText("jLabel25");
        jPanel6.add(cmp, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 240, 120, -1));

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel34.setText("No. Of Days :");
        jPanel6.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, -1, -1));

        days1.setText("jLabel19");
        jPanel6.add(days1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 240, -1, -1));

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel35.setText("Print Copy :");
        jPanel6.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 760, 70, 20));

        copy.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        copy.setText("jLabel36");
        jPanel6.add(copy, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 760, 70, 20));

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Key Received");
        jPanel6.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 510, 90, -1));

        jCheckBox1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBox1.setText("Yes");
        jPanel6.add(jCheckBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 510, -1, -1));

        jCheckBox2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBox2.setText("No");
        jPanel6.add(jCheckBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 510, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Bill Paid Seal");
        jPanel6.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, 90, -1));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setText("In Words :");
        jPanel6.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 60, -1));

        in_word.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        in_word.setText("jLabel7");
        jPanel6.add(in_word, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 590, 300, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Service Tax No. :");
        jPanel6.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, -1, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("AATPC1525FSD00L");
        jPanel6.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 610, -1, -1));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setText("273700676931");
        jPanel6.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 630, -1, -1));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("TIN No. :");
        jPanel6.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 630, -1, -1));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel32.setText("- All Bills payable on presentation.");
        jPanel6.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 660, -1, -1));

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel33.setText("- Rights Of Admission Reserved.");
        jPanel6.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, -1, -1));

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel36.setText("- Check out time 24 Hours.");
        jPanel6.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 700, -1, -1));

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel37.setText("- Subject to solapur Jurisdiction.");
        jPanel6.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 720, -1, -1));

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel39.setText("Guest Signature");
        jPanel6.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 730, -1, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("For - HOTEL VAISHNAVI");
        jPanel6.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 710, -1, -1));

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel40.setText("Authority Signatory");
        jPanel6.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 730, -1, -1));

        checkout_name.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        checkout_name.setText("jLabel7");
        jPanel6.add(checkout_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 640, 100, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("ChekOut By");
        jPanel6.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 620, 110, -1));

        jLabel28.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel28.setText("Paid Out");
        jPanel6.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 600, 50, 10));

        paid_out.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(paid_out, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 590, 100, 20));

        net_bill1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(net_bill1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 570, 100, 20));

        adv.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(adv, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 550, 100, 20));

        bill_amt.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(bill_amt, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 530, 100, 20));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel11.setText("Total");
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 540, 50, 10));

        jLabel26.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel26.setText("Advance");
        jPanel6.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 560, 50, 10));

        jLabel27.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel27.setText("Net Bill");
        jPanel6.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 580, 50, 10));

        d.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        d.setText("Discount");
        jPanel6.add(d, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 520, 50, 10));

        dis.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel6.add(dis, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 510, 60, 20));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 780, 600, -1));

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel38.setText("Software Developed & MainTain By Wavexito contact@8421127509");
        jPanel6.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 790, 390, -1));

        jPanel8.setBackground(new java.awt.Color(255, 246, 227));
        jPanel8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.black, java.awt.Color.black, java.awt.Color.black, java.awt.Color.black));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/LOGO.jpg"))); // NOI18N
        jPanel8.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 210, 70));

        jLabel42.setText("Email : bookings@hotelvaishnavisolapur.com");
        jPanel8.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 90, 260, 20));

        jLabel15.setText("93/C,GoldFinch Peth,Opp. Bhagwat Theater,Solapur- 413001");
        jPanel8.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 350, 20));

        jLabel16.setText("Tel :- 0217-2320541 , Mob :-07774073432 ");
        jPanel8.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 50, 250, 20));

        jLabel30.setText("website: hotelvaishnavisolapur.com");
        jPanel8.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 70, 210, 20));

        jPanel6.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 120));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(453, 557));
        setMinimumSize(new java.awt.Dimension(453, 557));
        getContentPane().setLayout(null);

        jPanel5.setBackground(new java.awt.Color(204, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.blue, java.awt.Color.white, java.awt.Color.black, java.awt.Color.blue));
        jPanel5.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setText("InVoice Bill");

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delete_1.jpg"))); // NOI18N
        jButton4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton4.setIconTextGap(10);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/print.jpg"))); // NOI18N
        jButton1.setText("Print");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.setIconTextGap(20);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(172, 172, 172)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)))
                    .addComponent(jButton1))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel5);
        jPanel5.setBounds(0, 0, 450, 50);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.blue, java.awt.Color.white, java.awt.Color.blue, java.awt.Color.white));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel12.setText("Invoice No.:");

        bill_no1.setText("bill");

        jLabel24.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel24.setText("Date :");

        dt1.setText("jLabel19");

        jPanel2.setLayout(new java.awt.BorderLayout());

        itable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item Name", "Quantity", "Rate", "Total"
            }
        ));
        jScrollPane1.setViewportView(itable);

        jLabel46.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel46.setText("Bill No.");

        ino.setText("jLabel47");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bill_no1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel46)
                        .addGap(49, 49, 49)
                        .addComponent(ino)
                        .addGap(92, 92, 92)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dt1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel12)
                        .addComponent(bill_no1)
                        .addComponent(jLabel46)
                        .addComponent(ino))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dt1)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(101, 101, 101)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(160, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 180, 450, 290);

        jLabel43.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel43.setText("Net Amount :");
        getContentPane().add(jLabel43);
        jLabel43.setBounds(210, 480, 71, 14);

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel44.setText("Customer Signature");
        getContentPane().add(jLabel44);
        jLabel44.setBounds(10, 520, 113, 14);

        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel45.setText("Manager Signature");
        getContentPane().add(jLabel45);
        jLabel45.setBounds(310, 520, 108, 14);

        jPanel3.setBackground(new java.awt.Color(204, 255, 255));

        shopname.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        shopname.setForeground(new java.awt.Color(255, 51, 102));
        shopname.setText("Shop name");

        mo_no.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        mo_no.setForeground(new java.awt.Color(255, 51, 102));
        mo_no.setText("Mobile");

        address.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        address.setForeground(new java.awt.Color(255, 51, 102));
        address.setText("Address");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(shopname, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(mo_no, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(shopname, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(mo_no, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 50, 450, 110);

        tot.setText("jLabel31");
        getContentPane().add(tot);
        tot.setBounds(290, 480, 40, 14);

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    /*public String convert(int number) {
  int n = 1;
  int word;
  string = "";
  while (number != 0) {
   switch (n) {
   case 1:
    word = number % 100;
    pass(word);
    if (number > 100 && number % 100 != 0) {
     show("and ");
    }
    number /= 100;
    break;
   case 2:
    word = number % 10;
    if (word != 0) {
     show(" ");
     show(st2[0]);
     show(" ");
     pass(word);
    }
    number /= 10;
    break;
   case 3:
    word = number % 100;
    if (word != 0) {
     show(" ");
     show(st2[1]);
     show(" ");
     pass(word);
    }
    number /= 100;
    break;
   case 4:
    word = number % 100;
    if (word != 0) {
     show(" ");
     show(st2[2]);
     show(" ");
     pass(word);
    }
    number /= 100;
    break;
   case 5:
    word = number % 100;
    if (word != 0) {
     show(" ");
     show(st2[3]);
     show(" ");
     pass(word);
    }
    number /= 100;
    break;
   }
   n++;
  }
  return string;
 }
*/
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        
      new secondpage().setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
           // st.executeUpdate("update reg set checkout='"+1+"' where cid='"+cid+"'");
            PrinterJob pj = PrinterJob.getPrinterJob();
            pj.setJobName("Subscriber Details for Msisdn=");
            pj.setCopies(1);
            PageFormat format = pj.defaultPage();
            format.setOrientation(PageFormat.LANDSCAPE);

            pj.setPrintable(new Printable() {
                public int print(Graphics pg, PageFormat pf, int pageNum){
                    if (pageNum > 0){
                        return Printable.NO_SUCH_PAGE;
                    }
                    Graphics2D g2 = (Graphics2D) pg;
                    g2.translate(pf.getImageableX(), pf.getImageableY());
                    jPanel6.paint(g2);
                    return Printable.PAGE_EXISTS;
                }
            });
            if (pj.printDialog() == false)
            return;

            pj.print();
           // Delux d=new Delux();
          //  d.setVisible(true);
            //this.dispose();
        }
        catch(Exception e)
        {
            System.err.print(e);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FinalInvoic.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FinalInvoic.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FinalInvoic.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FinalInvoic.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FinalInvoic().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address;
    private javax.swing.JLabel adv;
    private javax.swing.JLabel arr_dt;
    private javax.swing.JLabel arr_tm;
    private javax.swing.JLabel bill_amt;
    private javax.swing.JLabel bill_no;
    private javax.swing.JLabel bill_no1;
    private javax.swing.JLabel bn;
    private javax.swing.JLabel checkout_name;
    private javax.swing.JLabel cmp;
    private javax.swing.JLabel copy;
    private javax.swing.JLabel d;
    private javax.swing.JLabel days1;
    private javax.swing.JLabel dep_dt;
    private javax.swing.JLabel dep_tm;
    private javax.swing.JLabel dis;
    private javax.swing.JLabel dt;
    private javax.swing.JLabel dt1;
    private javax.swing.JLabel gn;
    private javax.swing.JLabel in_word;
    private javax.swing.JLabel ino;
    private javax.swing.JTable itable;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel label;
    private javax.swing.JLabel less_room;
    private javax.swing.JLabel mo_no;
    private javax.swing.JLabel net_bill1;
    private javax.swing.JLabel paid_out;
    private javax.swing.JLabel pax;
    private javax.swing.JLabel rn;
    private javax.swing.JLabel shopname;
    private javax.swing.JLabel tot;
    // End of variables declaration//GEN-END:variables
}
