/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package billingsystem;

import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Shashank
 */
public class IUDitem extends javax.swing.JFrame {

    /**
     * Creates new form IUDitem
     */ private Connection con;
     PreparedStatement pst;
     ResultSet rs;
    private int id_no;
    int getQty;
    public IUDitem() {
          setUndecorated(true);
        initComponents();
         this.pack();
        this.setLocationRelativeTo(null);
        initShow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        iname = new javax.swing.JLabel();
        iqty = new javax.swing.JLabel();
        irate = new javax.swing.JLabel();
        barcode = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        iratetxt = new javax.swing.JTextField();
        inametxt = new javax.swing.JTextField();
        iqtytxt = new javax.swing.JTextField();
        barcodetxt = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        item = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jTable1.setBackground(new java.awt.Color(204, 255, 255));
        jTable1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red, java.awt.Color.pink, java.awt.Color.red, java.awt.Color.pink));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id No", "Item Name", "Quantity", "Rate", "Barcode"
            }
        ));
        jTable1.setNextFocusableComponent(iname);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.darkGray, new java.awt.Color(102, 51, 255), java.awt.Color.darkGray, java.awt.Color.darkGray));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        iname.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        iname.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/mandatory.gif"))); // NOI18N
        iname.setText("Enter  Item Name");
        iname.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        iname.setIconTextGap(10);
        jPanel3.add(iname, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 150, 28));

        iqty.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        iqty.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/mandatory.gif"))); // NOI18N
        iqty.setText("Enter  Quantity");
        iqty.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        iqty.setIconTextGap(10);
        jPanel3.add(iqty, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 160, 28));

        irate.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        irate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/mandatory.gif"))); // NOI18N
        irate.setText("Enter Rate Per Quantity");
        irate.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        irate.setIconTextGap(10);
        jPanel3.add(irate, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 200, 28));

        barcode.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        barcode.setText("Enter Bar Code");
        barcode.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        barcode.setIconTextGap(10);
        jPanel3.add(barcode, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, 160, 28));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(66, 496, 142, 28));
        jPanel3.add(iratetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 100, 202, 30));
        jPanel3.add(inametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 202, 30));
        jPanel3.add(iqtytxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, 202, 30));
        jPanel3.add(barcodetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 150, 202, 30));

        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/pluseg.png"))); // NOI18N
        jButton1.setMnemonic('a');
        jButton1.setText("Add ");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });
        jPanel3.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 90, 40));

        jButton4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/update1.jpg"))); // NOI18N
        jButton4.setMnemonic('u');
        jButton4.setText("Update");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jButton4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton4KeyPressed(evt);
            }
        });
        jPanel3.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 220, 110, 40));

        jButton5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delet.png"))); // NOI18N
        jButton5.setMnemonic('d');
        jButton5.setText("Delete");
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jButton5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton5KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jButton5KeyReleased(evt);
            }
        });
        jPanel3.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 220, 110, 40));

        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/erase-128.png"))); // NOI18N
        jButton3.setMnemonic('c');
        jButton3.setText("Clear");
        jButton3.setNextFocusableComponent(item);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });
        jPanel3.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, 90, 40));

        jButton2.setBackground(new java.awt.Color(255, 204, 204));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton2.setMnemonic('h');
        jButton2.setText("Home");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });
        jPanel3.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, -1, 30));

        jButton7.setBackground(new java.awt.Color(255, 204, 204));
        jButton7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton7.setMnemonic('g');
        jButton7.setText("Go Back");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jButton7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton7KeyPressed(evt);
            }
        });
        jPanel3.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 300, 90, 30));

        jPanel1.setBackground(new java.awt.Color(255, 153, 204));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setText("Item Name");

        item.setNextFocusableComponent(jTable1);
        item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemActionPerformed(evt);
            }
        });
        item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                itemKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Copperplate Gothic Bold", 1, 16)); // NOI18N
        jLabel1.setText("Product Details");

        jButton6.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/update icon.png"))); // NOI18N
        jButton6.setMnemonic('r');
        jButton6.setText("Refresh");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delete.jpg"))); // NOI18N
        jButton8.setText("Close");
        jButton8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton8.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton8.setIconTextGap(10);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(415, 415, 415)
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(item, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jButton8)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(item, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void initShow(){
    try
        {
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            ResultSet rs=st.executeQuery("select item_name,price,qty,barcode from stock ");
            while(rs.next())
            {
                model.addRow(new Object[]{rs.getString(1),rs.getString(3),rs.getString(2),rs.getString(4)});
               
            }
            
        }
        catch(Exception e){
            System.out.println(e);
        }
}
    
    private void itemKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itemKeyReleased

        String nm;
        DefaultTableModel model;
        model=(DefaultTableModel)jTable1.getModel();
        model.setRowCount(0);
        String str;
        String str1="select * from stock where item_name LIKE ?";

        str=item.getText();
        try
        {
            con = DBConnection.getDBConnection();
            PreparedStatement pst=con.prepareStatement(str1);
            pst.setString(1,str+"%");
            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
                model.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
            }
        }
        catch(SQLException e)
        {
            System.err.println(e);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_itemKeyReleased

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new secondpage().setVisible(true);
                 this.setDefaultCloseOperation(secondpage.DISPOSE_ON_CLOSE);

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
        id_no=Integer.parseInt(model.getValueAt(jTable1.getSelectedRow(),0).toString());
        getQty=Integer.parseInt(model.getValueAt(jTable1.getSelectedRow(),2).toString());
        try{
            int row=jTable1.getSelectedRow();
            String t_click=(jTable1.getModel().getValueAt(row,0).toString());
            String sql="select * from stock where idstock='"+t_click+"'";
            pst=con.prepareCall(sql);
            rs=pst.executeQuery();
            if(rs.next()){
                String val2=rs.getString("item_name");
                inametxt.setText(val2);
                String val3=rs.getString("qty");
                iqtytxt.setText(val3);
               
                String val4=rs.getString("price");
                iratetxt.setText(val4);
                String val5=rs.getString("barcode");
                barcodetxt.setText(val5);
            }
            
        }catch(Exception e){}
        
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if(inametxt.getText().equalsIgnoreCase("") || iqtytxt.getText().equalsIgnoreCase("") || iratetxt.getText().equalsIgnoreCase(""))
        {
            JOptionPane.showMessageDialog (null, "Please Check all fields entered properly ", "Add Item Mesg", JOptionPane.ERROR_MESSAGE);
           
        }
        else{
            DefaultTableModel model=(DefaultTableModel) jTable1.getModel();

            String itemname,quantity,rate,barcode;
            itemname=inametxt.getText();
            quantity=iqtytxt.getText();
            rate=iratetxt.getText();
            barcode=barcodetxt.getText();
            try{
                //int key=0;
                con=DBConnection.getDBConnection();
                Statement pst=con.createStatement();
                 model.addRow(new Object[]{itemname,quantity,rate,barcode});
                pst.executeUpdate("insert into stock(item_name,qty,price,barcode) values('"+itemname+"','"+quantity+"','"+rate+"','"+barcode+"')");
                JOptionPane.showMessageDialog (null, "Item added Successsful", "Add Item msg", JOptionPane.INFORMATION_MESSAGE);
             
            }catch(SQLException | HeadlessException e){
                System.out.println(e);
            }

        }//else end
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        try
        {
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            String itemname,rate,barcode;
            int quantity;
            itemname=inametxt.getText();
            int quan=Integer.parseInt(iqtytxt.getText());
            quantity=getQty+quan;
                    
            rate=iratetxt.getText();
            barcode=barcodetxt.getText();
            st.executeUpdate("update stock set item_name='"+itemname+"',qty='"+quantity+"',price='"+rate+"',barcode='"+barcode+"'  where idstock='"+id_no+"'");
            System.out.println(id_no);
            JOptionPane.showMessageDialog (null," Data Updated Successfully", "Item Updated ", JOptionPane.INFORMATION_MESSAGE);
           
        }
        catch(Exception e)
        {
            System.out.println(e);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

        DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
        try{
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            st.executeUpdate("delete from stock where idstock='"+id_no+"'");
            JOptionPane.showMessageDialog (null," Item Deleted Successfully", "Item Deleted", JOptionPane.INFORMATION_MESSAGE);
           // new secondpage().setVisible(true);
            System.out.println(id_no);
            model.removeRow(jTable1.getSelectedRow());
             
        }
        catch(Exception e)
        {
            System.out.println(e);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void itemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_itemActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new adminpanel().setVisible(true);
        this.setDefaultCloseOperation(adminpanel.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            if(inametxt.getText().equalsIgnoreCase("") || iqtytxt.getText().equalsIgnoreCase("") || iratetxt.getText().equalsIgnoreCase(""))
        {
            JOptionPane.showMessageDialog (null, "Please Check all fields entered properly ", "Add Item Mesg", JOptionPane.ERROR_MESSAGE);
            this.dispose();
        }
        else{
  DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
            String itemname,quantity,rate,barcode;
            itemname=inametxt.getText();
            quantity=iqtytxt.getText();
            rate=iratetxt.getText();
            barcode=barcodetxt.getText();
            try{
              
                con=DBConnection.getDBConnection();
                Statement pst=con.createStatement();
                model.addRow(new Object[]{itemname,quantity,rate,barcode});
                pst.executeUpdate("insert into stock(item_name,qty,price,barcode) values('"+itemname+"','"+quantity+"','"+rate+"','"+barcode+"')");
                JOptionPane.showMessageDialog (null, "Item added Successsful", "Add Item msg", JOptionPane.INFORMATION_MESSAGE);
             
            }catch(SQLException | HeadlessException e){
                System.out.println(e);
            }

        }
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton4KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
             try
        {
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            String itemname,quantity,rate,barcode;
            itemname=inametxt.getText();
            quantity=iqtytxt.getText();
            rate=iratetxt.getText();
            barcode=barcodetxt.getText();
            st.executeUpdate("update stock set item_name='"+itemname+"',qty='"+quantity+"',price='"+rate+"',barcode='"+barcode+"'  where idstock='"+id_no+"'");
            System.out.println(id_no);
            JOptionPane.showMessageDialog (null," Data Updated Successfully", "Item Updated ", JOptionPane.INFORMATION_MESSAGE);
           
        }
        catch(Exception e)
        {
            System.out.println(e);
        }

         }
    }//GEN-LAST:event_jButton4KeyPressed

    private void jButton5KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton5KeyReleased
        // TODO add your handling code here:
               DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
        try{
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            st.executeUpdate("delete from stock where idstock='"+id_no+"'");
            JOptionPane.showMessageDialog (null," Item Deleted Successfully", "Item Deleted", JOptionPane.INFORMATION_MESSAGE);
           // new secondpage().setVisible(true);
            System.out.println(id_no);
            model.removeRow(jTable1.getSelectedRow());
             
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }//GEN-LAST:event_jButton5KeyReleased

    private void jButton5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton5KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
             
         }
    }//GEN-LAST:event_jButton5KeyPressed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        this.dispose();
        new secondpage().setVisible(true);
                 this.setDefaultCloseOperation(secondpage.DISPOSE_ON_CLOSE);
    }
    }//GEN-LAST:event_jButton2KeyPressed

    private void jButton7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton7KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
             this.dispose();
        new adminpanel().setVisible(true);
        this.setDefaultCloseOperation(adminpanel.DISPOSE_ON_CLOSE);
         }
    }//GEN-LAST:event_jButton7KeyPressed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            inametxt.setText("");
            iqtytxt.setText("");
            iratetxt.setText("");
            barcodetxt.setText("");
        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        inametxt.setText("");
        iqtytxt.setText("");
        iratetxt.setText("");
        barcodetxt.setText("");

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_DOWN ||evt.getKeyCode()==KeyEvent.VK_UP || evt.getKeyCode()==KeyEvent.VK_ENTER ){
      
        DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
        inametxt.setText(model.getValueAt(jTable1.getSelectedRow(),1).toString());
        iqtytxt.setText(model.getValueAt(jTable1.getSelectedRow(),3).toString());
        iratetxt.setText(model.getValueAt(jTable1.getSelectedRow(),2).toString());
        barcodetxt.setText(model.getValueAt(jTable1.getSelectedRow(),4).toString());  ///sum of total column
    }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        try
        {
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            ResultSet rs=st.executeQuery("select item_name,price,qty,barcode from stock ");
            while(rs.next())
            {
                model.addRow(new Object[]{rs.getString(1),rs.getString(3),rs.getString(2),rs.getString(4)});

            }
            //this.dispose();
            // new main_dashboard().setVisible(true);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton8ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IUDitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IUDitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IUDitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IUDitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IUDitem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel barcode;
    private javax.swing.JTextField barcodetxt;
    private javax.swing.JLabel iname;
    private javax.swing.JTextField inametxt;
    private javax.swing.JLabel iqty;
    private javax.swing.JTextField iqtytxt;
    private javax.swing.JLabel irate;
    private javax.swing.JTextField iratetxt;
    private javax.swing.JTextField item;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
