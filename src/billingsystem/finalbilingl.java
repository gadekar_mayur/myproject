
package billingsystem;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class finalbilingl extends javax.swing.JFrame {
 private Connection con;
    private int cid;
    private int roomtype;
    private int room;
    private PreparedStatement preStatement;
    private PreparedStatement preStatement1;
    String d1;
    private Component panel;
    private int flag;
    private String time1;
    private String time2;
    private int total_amt;
    private int main_total;
    private int paid;
    private int pen;
    private String status;
    private int paiduse;
    private int penuse;
    private int statususe;
    private String dateuse;
    private String service_date;
    private String service_time;
    private int id;
    private int id1;
    private int flag1;
      String date;
      int bill_no=0;
      int count = 0;
      int getTax =0;
      int tqty;
      double sum;
      double totamt=0;
       double discAmt=0;
       double taxAmt=0;
       double cal;
       double caldis;
       int selectqty;
       int getQty;
       double totrate,rate;
       double gettot;
       double paid_amt;
       double pend_amt;
       int stock;
   
    public finalbilingl() {
          setUndecorated(true);
        initComponents();
         this.pack();
        this.setLocationRelativeTo(null);
        DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
        date=dateformat.format(new Date());
        dt.setText(date);
                initshow();
               String getTax = taxamt.getText();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        item1 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        list1 = new javax.swing.JList<>();
        badd = new javax.swing.JButton();
        bupdate = new javax.swing.JButton();
        bdelete = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        tbqty = new javax.swing.JTextField();
        msg = new javax.swing.JTextField();
        csave = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        sto = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        itable = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        bno = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cname = new javax.swing.JTextField();
        mob = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        list2 = new javax.swing.JList<>();
        dt = new javax.swing.JLabel();
        dt1 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        netb = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        icnt = new javax.swing.JTextField();
        tamt = new javax.swing.JTextField();
        taxamt = new javax.swing.JTextField();
        dis = new javax.swing.JTextField();
        rbno = new javax.swing.JTextField();
        dAmt = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        taxinclud = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        qtyt = new javax.swing.JTextField();
        Net1 = new javax.swing.JButton();
        netAmt = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        pend1 = new javax.swing.JTextField();
        paidAmt1 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        stat = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(836, 601));
        setMinimumSize(new java.awt.Dimension(836, 601));

        jPanel1.setBackground(new java.awt.Color(255, 153, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.darkGray, java.awt.Color.gray, java.awt.Color.darkGray, java.awt.Color.lightGray));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/billingsystem/rupees.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Copperplate Gothic Bold", 1, 18)); // NOI18N
        jLabel2.setText("Bill Entry");

        jButton7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delete.jpg"))); // NOI18N
        jButton7.setText("Close");
        jButton7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton7.setIconTextGap(10);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red, java.awt.Color.darkGray, java.awt.Color.blue, java.awt.Color.darkGray));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("Item Name");

        item1.setNextFocusableComponent(list1);
        item1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                item1MouseClicked(evt);
            }
        });
        item1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                item1ActionPerformed(evt);
            }
        });
        item1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                item1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                item1KeyReleased(evt);
            }
        });

        list1.setNextFocusableComponent(tbqty);
        list1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list1MouseClicked(evt);
            }
        });
        list1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                list1PropertyChange(evt);
            }
        });
        list1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                list1KeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(list1);

        badd.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        badd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/pluseg.png"))); // NOI18N
        badd.setMnemonic('a');
        badd.setText("Add");
        badd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        badd.setIconTextGap(10);
        badd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                baddActionPerformed(evt);
            }
        });
        badd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                baddKeyPressed(evt);
            }
        });

        bupdate.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bupdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/update.png"))); // NOI18N
        bupdate.setMnemonic('u');
        bupdate.setText("Update");
        bupdate.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bupdate.setIconTextGap(10);
        bupdate.setNextFocusableComponent(itable);
        bupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bupdateActionPerformed(evt);
            }
        });
        bupdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bupdateKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                bupdateKeyTyped(evt);
            }
        });

        bdelete.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bdelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delete1.jpg"))); // NOI18N
        bdelete.setMnemonic('d');
        bdelete.setText("Delete");
        bdelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bdelete.setIconTextGap(10);
        bdelete.setNextFocusableComponent(itable);
        bdelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bdeleteActionPerformed(evt);
            }
        });
        bdelete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bdeleteKeyPressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Quantity");

        tbqty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbqtyActionPerformed(evt);
            }
        });

        msg.setForeground(new java.awt.Color(255, 51, 51));
        msg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                msgActionPerformed(evt);
            }
        });

        csave.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        csave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/print.jpg"))); // NOI18N
        csave.setMnemonic('b');
        csave.setText("Save and Bill");
        csave.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        csave.setIconTextGap(10);
        csave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                csaveActionPerformed(evt);
            }
        });
        csave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                csaveKeyPressed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 204, 204));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton2.setMnemonic('h');
        jButton2.setText("Home");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel17.setText("Available Stock");

        sto.setForeground(new java.awt.Color(255, 51, 51));
        sto.setText("NaN");

        itable.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.cyan, java.awt.Color.darkGray, java.awt.Color.cyan, java.awt.Color.black));
        itable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Item Name", "Rate", "Qty", "Total"
            }
        ));
        itable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                itableMouseClicked(evt);
            }
        });
        itable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itableKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itableKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(itable);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(item1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sto, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tbqty, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(badd)
                                .addGap(19, 19, 19)
                                .addComponent(bupdate)
                                .addGap(19, 19, 19)
                                .addComponent(bdelete))
                            .addComponent(msg, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jButton2)
                        .addGap(27, 27, 27)
                        .addComponent(csave)))
                .addContainerGap(35, Short.MAX_VALUE))
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(2, 2, 2)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tbqty, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(bupdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bdelete)
                                .addComponent(badd, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(msg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(item1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, 0)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(sto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(108, 108, 108)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(csave))
                .addGap(84, 84, 84))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel10, jLabel7});

        item1.getAccessibleContext().setAccessibleName("i");

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red, java.awt.Color.lightGray, java.awt.Color.blue, java.awt.Color.darkGray));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel3.setText("Bill No.");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel5.setText("Customer Name");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel6.setText("Mo No.");

        bno.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bno.setText("jLabel8");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel4.setText("Date/Time");

        cname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cnameActionPerformed(evt);
            }
        });
        cname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cnameKeyReleased(evt);
            }
        });

        mob.setNextFocusableComponent(jButton3);
        mob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mobActionPerformed(evt);
            }
        });
        mob.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                mobKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                mobKeyReleased(evt);
            }
        });

        list2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list2MouseClicked(evt);
            }
        });
        list2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                list2PropertyChange(evt);
            }
        });
        list2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list2KeyPressed(evt);
            }
        });
        list2.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                list2ValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(list2);

        dt.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        dt.setText("jLabel18");

        dt1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        dt1.setText("jLabel18");

        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jButton3.setForeground(new java.awt.Color(204, 0, 102));
        jButton3.setText("Add ,If new Customer");
        jButton3.setNextFocusableComponent(item1);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(297, 297, 297)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(107, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bno, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dt1)
                            .addComponent(dt))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cname)
                    .addComponent(mob, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bno)
                    .addComponent(jLabel5))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dt1)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cname, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(mob, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setLayout(null);

        netb.setBackground(new java.awt.Color(255, 255, 255));
        netb.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.pink, java.awt.Color.pink, java.awt.Color.pink, java.awt.Color.pink));
        netb.setMinimumSize(new java.awt.Dimension(560, 700));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel8.setText("No. of Item");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel9.setText("Total Amount");

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel11.setText("Tax(%)");

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel12.setText("Discount(%)");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel13.setText("Return Bill No.");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel15.setText("Disc Amt");

        icnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                icntActionPerformed(evt);
            }
        });

        taxamt.setText("0");
        taxamt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxamtActionPerformed(evt);
            }
        });
        taxamt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                taxamtKeyReleased(evt);
            }
        });

        dis.setText("0");
        dis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disActionPerformed(evt);
            }
        });
        dis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                disKeyReleased(evt);
            }
        });

        rbno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbnoActionPerformed(evt);
            }
        });

        dAmt.setText("0");
        dAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dAmtActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel19.setText("Tax includes");

        taxinclud.setText("0");

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel20.setText("Total  Qty");

        Net1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        Net1.setMnemonic('n');
        Net1.setText("Net  Bill ");
        Net1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Net1ActionPerformed(evt);
            }
        });
        Net1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Net1KeyPressed(evt);
            }
        });

        netAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                netAmtActionPerformed(evt);
            }
        });
        netAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                netAmtKeyReleased(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel33.setText("Paid Amount");

        jLabel34.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel34.setText("Pendding");

        pend1.setText("0");
        pend1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pend1ActionPerformed(evt);
            }
        });

        paidAmt1.setText("0");
        paidAmt1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                paidAmt1KeyReleased(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel35.setText("Status");

        stat.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        stat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pendding", "Paid" }));
        stat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel16.setText("Net Sales");

        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout netbLayout = new javax.swing.GroupLayout(netb);
        netb.setLayout(netbLayout);
        netbLayout.setHorizontalGroup(
            netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, netbLayout.createSequentialGroup()
                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(netbLayout.createSequentialGroup()
                                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(taxinclud, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
                            .addGroup(netbLayout.createSequentialGroup()
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tamt, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(taxamt, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(dis, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(rbno, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(dAmt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)))
                            .addGroup(netbLayout.createSequentialGroup()
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(netbLayout.createSequentialGroup()
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(51, 51, 51))
                                    .addGroup(netbLayout.createSequentialGroup()
                                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(icnt)
                                    .addComponent(qtyt)))
                            .addGroup(netbLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(44, 44, 44))
                    .addGroup(netbLayout.createSequentialGroup()
                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(stat, 0, 109, Short.MAX_VALUE)
                            .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(paidAmt1)
                                .addComponent(pend1, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(netbLayout.createSequentialGroup()
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(netbLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Net1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(netAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(netbLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(netbLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        netbLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dAmt, dis, icnt, jTextField8, rbno, tamt, taxamt, taxinclud});

        netbLayout.setVerticalGroup(
            netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(icnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(qtyt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(taxamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(taxinclud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Net1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(netAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(paidAmt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel34)
                    .addComponent(pend1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addComponent(netb, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 541, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(netb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
       public void additem(String cust_name){
           DefaultListModel listmodel2=new DefaultListModel();
           list2.setModel(listmodel2);
           listmodel2.addElement(cust_name);
       }
       public void addtable()
       {
           int cid = 0;
                       int rowsCount = itable.getRowCount();
       try{
       con = DBConnection.getDBConnection();
       Statement st,st1,st2,st3,st4;
       st=con.createStatement();
       String cust_name=cname.getText();
           ResultSet  rs,rs1,rs2,rs3,rs4,rs5;
           rs=st.executeQuery("select cid from customer_info_table where cust_name='"+cust_name+"'");
                while(rs.next()){
                     cid=rs.getInt(1);
                }
                System.out.println("cid addTable "+cid);
       String bilno=bno.getText();
       System.out.println("bill no add Table"+bilno);
           for(int i = 0; i < itable.getRowCount(); i++){
               String id=(itable.getValueAt(i,0).toString());
               int parseid=Integer.parseInt(id);
               String name=(itable.getValueAt(i,1).toString());
               String price=(itable.getValueAt(i,2).toString());
               //double price2=Double.parseDouble(price);
               int mrp=(int) Double.parseDouble(price);
               String qty=(itable.getValueAt(i,3).toString());//after order taken(sales)2 - actual qty(stock)10
               int parseqty=Integer.parseInt(qty);
               System.out.println("parse user quantity ="+parseqty);
               String total=(itable.getValueAt(i,4).toString());
               st.executeUpdate("insert into sales (cid,item_id,bno,item_name,price,qty,total) values('"+cid+"','"+id+"','"+bilno+"','"+name+"','"+mrp+"','"+qty+"','"+total+"')");
              }
           
                st1=con.createStatement();
               rs1=st1.executeQuery("select * from sales where bno='"+bilno+"'");
               while(rs1.next())
               {
                   int item_id=rs1.getInt("item_id");
                   int order_qty=rs1.getInt("qty");
                   st2=con.createStatement();
               rs2=st2.executeQuery("select * from stock where idstock='"+item_id+"'");
               while(rs2.next())
               {
                   int stock_qty=rs2.getInt("qty");
                   int final_qty=stock_qty-order_qty;
                   st3=con.createStatement();
               st3.executeUpdate("Update stock set qty='"+final_qty+"' where idstock='"+item_id+"'");
               
               System.out.println("Stock updated successfully finalBiling 894");
               }
                   
               }
               
           System.out.println("End of for loop");
           }
         catch(SQLException e)
        {
            System.out.println("db stock updated");
            e.printStackTrace();
        } 
       
       }
        public int getsum(){
                   int rowsCount = itable.getRowCount();
                    sum = 0.0;
                    tqty=0;
                for(int i = 0; i < rowsCount; i++){
                                            sum = sum+Double.parseDouble(itable.getValueAt(i,4).toString());
                                          sum=Double.parseDouble(new DecimalFormat("##.####").format(sum));
                                            tqty=tqty+Integer.parseInt(itable.getValueAt(i,3).toString());
                                                    }
                                            tamt.setText(Double.toString((double) sum));
                                            qtyt.setText(Integer.toString(tqty));
                                            System.out.println("sum"+sum);
     return tqty;
                            }

    
    
    public void initshow(){
        cname.setText("");
        mob.setText("");
        Calendar cal = Calendar.getInstance();
        
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        time2=sdf1.format(cal.getTime());
        System.out.println(time1);
       dt1.setText(String.valueOf(sdf1.format(cal.getTime())));
       
       
       try{
       con = DBConnection.getDBConnection();
       Statement st=con.createStatement();
       ResultSet rst1=st.executeQuery("SELECT bill_no FROM bill_table ORDER BY bill_no DESC LIMIT 1");
               if(rst1.next()){
               
                      id1=rst1.getInt(1)+1;
                                }
               else
               {
                 id1=1;
               }
               bno.setText(String.valueOf(id1));
               rbno.setText(String.valueOf(id1));

       }
         catch(SQLException e)
        {
            System.out.println(" Bill No Increment");
            e.printStackTrace();
        } 
        
}    
    private void baddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_baddActionPerformed
      
       if(tbqty.getText().equalsIgnoreCase("")|| item1.getText().equalsIgnoreCase(""))
        {
            JOptionPane.showMessageDialog (null, "Please Enter all Fields Item Name,Quantity ", "Data Fill", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            ResultSet rs=null;
             int count = 0;
            String s1=item1.getText();
            try{
                con = DBConnection.getDBConnection();                                          //
             
                PreparedStatement ps = con.prepareStatement("select price,idstock,qty from stock where item_name=?");
                ps.setString(1,s1);
                rs=ps.executeQuery();
                while(rs.next()){
                     double mrp=Double.parseDouble(rs.getString(1));
             System.out.println("mrp="+mrp);
                    int s3=rs.getInt(2);
                    int avai_stock=rs.getInt(3);
             
                    int qty= Integer.parseInt(tbqty.getText());
                    System.out.println("qty="+qty);
                    if(qty>avai_stock)
                    {
                        JOptionPane.showMessageDialog (null, "Sorry, This much Stock is not available ", "Data Fill", JOptionPane.ERROR_MESSAGE);
                        break;
                   }
                    double total=qty*mrp;
                    System.out.println("total="+total);
           msg.setText("");
                    DefaultTableModel model=(DefaultTableModel) itable.getModel();
                    if(tbqty.getText().trim().equals(""))
                    {
                        msg.setText("Quantity must not be blank");
                    }else {
                     
                         model.addRow(new Object[]{s3,item1.getText().toString(),rs.getString(1),tbqty.getText(),total});
                         flag1=1;
                        count = itable.getRowCount();
                    }  
                    
                    icnt.setText(String.valueOf(count));
                    getsum();
                    item1.setText("");
                    tbqty.setText("");
                //compli.setSelectedIndex(0);
                jScrollPane3.setVisible(false);
                }  
        }catch (SQLException ex) {
               Logger.getLogger(finalbilingl.class.getName()).log(Level.SEVERE, null, ex);
        }    }
    }//GEN-LAST:event_baddActionPerformed

    private void bupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bupdateActionPerformed
                    msg.setText("");
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty.");
            }else{
                msg.setText("You must select a row.");
            }
        }
        else
        {
            int user=0;
              selectqty=Integer.parseInt(tbqty.getText());
          //  int qty=Integer.parseInt(tbqty.getText());
            String iname=item1.getText();
            
            model.setValueAt(item1.getText().toString(),itable.getSelectedRow(),1);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from stock where item_name='"+iname+"'");
                while(rst1.next())
                {
                    user=rst1.getInt(4);             //user saving value of price from db
                }
            }catch(Exception e){}
            model.setValueAt(user,itable.getSelectedRow(),2);
            int total=user*selectqty;
            model.setValueAt(tbqty.getText(),itable.getSelectedRow(),3);
        
            model.setValueAt(total,itable.getSelectedRow(),4);
            msg.setText("Data Updated Successfully");
            
              System.out.println(".rate of selected row item....."+rate);
            getQty = Integer.parseInt(qtyt.getText());
          getsum();
             item1.setText("");
             tbqty.setText("");
            
        }
    }//GEN-LAST:event_bupdateActionPerformed

    private void bdeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bdeleteActionPerformed
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty");
            }else{
                msg.setText("You must select a row");
            }
        }else{
           System.out.println("......"+rate);
            System.out.println(item1.getText());
            selectqty=Integer.parseInt(tbqty.getText());
            getQty = Integer.parseInt(qtyt.getText());
           System.out.println("tbqty.getText()"+getQty);
            qtyt.setText(String.valueOf(getQty-selectqty));
            gettot= Integer.parseInt(tamt.getText());
            tamt.setText(String.valueOf(gettot-totrate));
            model.removeRow(itable.getSelectedRow());
             int count2 = itable.getSelectedColumnCount();
             icnt.setText(String.valueOf(count2-count));
             item1.setText("");
             tbqty.setText("");
        }

    }//GEN-LAST:event_bdeleteActionPerformed

    private void csaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_csaveActionPerformed
         DefaultTableModel model=(DefaultTableModel) itable.getModel();
                  try{  
          ResultSet rs,rs1;

               
                String  cust_mob=mob.getText();
                Double Netamt=Double.parseDouble(netAmt.getText());
                System.out.println("net bill+netAmt.getText()"+Netamt);
                int bn=Integer.parseInt(bno.getText());
                System.out.println("bill no+bno.getText()"+bn);
                String dat=dt.getText();
                System.out.println("date+dt.getText()"+dat);
                double grandtot=Double.parseDouble(tamt.getText());
                System.out.println("tot amt+tamt.getText()"+grandtot);
                double disc=Double.parseDouble(dis.getText());
                System.out.println("dis %+dis.getText()"+disc);
                double paidamount=Double.parseDouble(paidAmt1.getText());
                System.out.println("p amt+paidAmt1.getText()"+paidamount);
                double pendding=Double.parseDouble(pend1.getText());
                System.out.println("pend+pend1.getText()"+pendding);
                double tax=Double.parseDouble(taxamt.getText());
                System.out.println("dis %+dis.getText()"+tax);
                System.out.println("tax %"+taxamt.getText());
                String cust_name=cname.getText();
                System.out.println("cust cname"+cust_name);
                System.out.println("hiiiiiiiiiiii");
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
               addtable();
                rs=st.executeQuery("select cid from customer_info_table where cust_name='"+cust_name+"'");
                while(rs.next()){
                     cid=rs.getInt(1);
                     System.out.println("cid inside while loop"+cid);
                }
                System.out.println("cid"+cid);
                st.executeUpdate("insert into bill_table(bill_no,bill_date,grand_bill,discount,net_bill,paid_amt,pending_amt,status,cid,tax)values('"+bn+"','"+dat+"','"+grandtot+"','"+disc+"','"+Netamt+"','"+paidamount+"','"+pendding+"','"+status+"','"+cid+"','"+tax+"')");
               bill_no=Integer.parseInt(bno.getText());
                System.out.println("This Final one bill no  ="+bill_no);
                FinalInvoic i=new  FinalInvoic(cid,bill_no);
                JOptionPane.showMessageDialog(panel,"Successfully Saved");  
              new FinalInvoic().setVisible(true);
            }
        catch(Exception ex){
            System.out.println(ex);
        }        
        
    }//GEN-LAST:event_csaveActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new secondpage().setVisible(true);
        this.setDefaultCloseOperation(secondpage.DISPOSE_ON_CLOSE);
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void msgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_msgActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_msgActionPerformed

    private void taxamtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxamtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_taxamtActionPerformed

    private void disActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_disActionPerformed

    private void rbnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbnoActionPerformed

    private void dAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dAmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dAmtActionPerformed

    private void tbqtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbqtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbqtyActionPerformed

    private void itableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_itableMouseClicked
       int qty;
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
        tbqty.setText(model.getValueAt(itable.getSelectedRow(),3).toString());
        
        rate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),2).toString());
        totrate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),4).toString());   ///sum of total column
       
    }//GEN-LAST:event_itableMouseClicked

    private void icntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_icntActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_icntActionPerformed

    private void Net1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Net1ActionPerformed
        // TODO add your handling code here:
        taxAmt= (Double.parseDouble(taxinclud.getText()));
        totamt= (Double.parseDouble(tamt.getText()));    
        discAmt=(Double.parseDouble(dAmt.getText()));    
        System.out.println("total amt"+totamt);
        System.out.println("cal"+taxAmt);
        System.out.println("caldis"+discAmt);
        double net_amt=(totamt+taxAmt-discAmt);
        double val = Math.round(net_amt);
        System.out.println("Net amt"+net_amt);
        netAmt.setText(String.valueOf(val));
        jTextField8.setText(String.valueOf(net_amt));
    }//GEN-LAST:event_Net1ActionPerformed

    private void netAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_netAmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_netAmtActionPerformed

    private void paidAmt1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paidAmt1KeyReleased
        // TODO add your handling code here:
        double getNet=(Double.parseDouble(netAmt.getText()));
        System.out.println("getNet"+getNet);
            double getPaid=(Double.parseDouble(paidAmt1.getText()));
            System.out.println("Get Paid amt"+getPaid);
            System.out.println(getPaid);
            double calPend=getNet-getPaid;
        pend1.setText(String.valueOf(calPend));
        if(pend1.getText().equals("0.0")){
                    stat.setSelectedIndex(1);
                    status="Paid";
                    }
                else{
                    stat.setSelectedIndex(0);
                    status="Pendding";
                    
                }
    }//GEN-LAST:event_paidAmt1KeyReleased

    private void taxamtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxamtKeyReleased
        // TODO add your handling code here:
          
                     totamt=Integer.parseInt(tamt.getText());
                     taxAmt= (Double.parseDouble(taxamt.getText())/100)*totamt;
                     taxAmt=Double.parseDouble(new DecimalFormat("##.####").format(taxAmt));
                     System.out.println("tax%="+taxAmt);
                     cal=totamt-taxAmt;  
                     taxinclud.setText(String.valueOf(taxAmt));
                    
    }//GEN-LAST:event_taxamtKeyReleased

    private void disKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_disKeyReleased
        // TODO add your handling code here:
        
                     totamt=Integer.parseInt(tamt.getText());
                     discAmt= (Double.parseDouble(dis.getText())/100)*totamt;
                     discAmt=Double.parseDouble(new DecimalFormat("##.####").format(discAmt));
                     caldis=totamt-discAmt;  
                     dAmt.setText(String.valueOf(discAmt));
    }//GEN-LAST:event_disKeyReleased

    private void netAmtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_netAmtKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_netAmtKeyReleased

    private void cnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cnameActionPerformed
        // TODO add your handling code here:
        cname.setText(list2.getSelectedValue());
        flag=1;
        jScrollPane3.setVisible(false);
        mob.requestFocusInWindow();
    }//GEN-LAST:event_cnameActionPerformed

    private void cnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cnameKeyReleased
        // TODO add your handling code here:
        DefaultListModel listmodel2=new DefaultListModel();
        jScrollPane3.setVisible(true);

        String str;
     

        String str1="select cust_name,cust_mobile from customer_info_table where cust_name LIKE ?";

        str=cname.getText();
        try
        {
            con = DBConnection.getDBConnection();
            PreparedStatement pst=con.prepareStatement(str1);
            pst.setString(1,str+"%");
     
            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
                listmodel2.addElement(rs.getString(1));
                mob.setText(rs.getString(2));

            }
            list2.setModel(listmodel2);
            // list1.setSelectedIndex(0);
            if(listmodel2.isEmpty())
            {

            }
            else
            list2.setSelectedValue(listmodel2.lastElement(), true);
        
        }
        
        catch(SQLException e)
        {
            System.out.println(e);
        }
    }//GEN-LAST:event_cnameKeyReleased

    private void mobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mobActionPerformed
        // TODO add your handling code here:
        item1.requestFocusInWindow();
    }//GEN-LAST:event_mobActionPerformed

    private void list2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list2MouseClicked
        // TODO add your handling code here:
        cname.setText(list2.getSelectedValue());
        flag=1;
        jScrollPane3.setVisible(false);
        mob.requestFocusInWindow();
    }//GEN-LAST:event_list2MouseClicked

    private void list2PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_list2PropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_list2PropertyChange

    private void baddKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_baddKeyPressed
        // TODO add your handling code here:
                if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                          
       if(tbqty.getText().equalsIgnoreCase("")|| item1.getText().equalsIgnoreCase(""))
        {
            JOptionPane.showMessageDialog (null, "Please Enter all Fields Item Name,Quantity ", "Data Fill", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            ResultSet rs=null;
             int count = 0;
            String s1=item1.getText();
            try{
                con = DBConnection.getDBConnection();                                          //
             
                PreparedStatement ps = con.prepareStatement("select price,idstock,qty from stock where item_name=?");
                ps.setString(1,s1);
                rs=ps.executeQuery();
                while(rs.next()){
                     int mrp=Integer.parseInt(rs.getString(1));
                   // System.out.println("MRP="+mrp);
                    int s3=rs.getInt(2);
                    int avai_stock=rs.getInt(3);
                    //  System.out.println("stock id="+s3);
                    //String barcod=rs.getString(3);
                    int qty=Integer.parseInt(tbqty.getText());
                    System.out.println("qty="+qty);
                    if(qty>avai_stock)
                    {
                        JOptionPane.showMessageDialog (null, "Sorry, This much Stock is not available ", "Data Fill", JOptionPane.ERROR_MESSAGE);
                        break;
                   }
                    int total=qty*mrp;
           msg.setText("");
                    DefaultTableModel model=(DefaultTableModel) itable.getModel();
                    if(tbqty.getText().trim().equals(""))
                    {
                        msg.setText("Quantity must not be blank");
                    }else {
                     
                         model.addRow(new Object[]{s3,item1.getText().toString(),rs.getString(1),tbqty.getText(),total});
                         flag1=1;
                        count = itable.getRowCount();
                    }  
                    
                    icnt.setText(String.valueOf(count));
                    getsum();
                    item1.setText("");
                    tbqty.setText("");
                //compli.setSelectedIndex(0);
                jScrollPane3.setVisible(false);
                }  
        }catch (SQLException ex) {
               Logger.getLogger(finalbilingl.class.getName()).log(Level.SEVERE, null, ex);
        }    }
                }
    }//GEN-LAST:event_baddKeyPressed

    private void bupdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bupdateKeyPressed
        // TODO add your handling code here:
                if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                              msg.setText("");
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty.");
            }else{
                msg.setText("You must select a row.");
            }
        }
        else
        {
            int user=0;
              selectqty=Integer.parseInt(tbqty.getText());
          //  int qty=Integer.parseInt(tbqty.getText());
            String iname=item1.getText();
            
            model.setValueAt(item1.getText().toString(),itable.getSelectedRow(),1);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from stock where item_name='"+iname+"'");
                while(rst1.next())
                {
                    user=rst1.getInt(4);             //user saving value of price from db
                }
            }catch(Exception e){}
            model.setValueAt(user,itable.getSelectedRow(),2);
            int total=user*selectqty;
            model.setValueAt(tbqty.getText(),itable.getSelectedRow(),3);
        
            model.setValueAt(total,itable.getSelectedRow(),4);
            msg.setText("Data Updated Successfully");
            
              System.out.println(".rate of selected row item....."+rate);
            getQty = Integer.parseInt(qtyt.getText());
          getsum();
             item1.setText("");
             tbqty.setText("");
            
        }
                }
    }//GEN-LAST:event_bupdateKeyPressed

    private void bdeleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bdeleteKeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_DELETE){
                     DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty");
            }else{
                msg.setText("You must select a row");
            }
        }else{
           System.out.println("......"+rate);
            System.out.println(item1.getText());
            selectqty=Integer.parseInt(tbqty.getText());
            getQty = Integer.parseInt(qtyt.getText());
           System.out.println("tbqty.getText()"+getQty);
            qtyt.setText(String.valueOf(getQty-selectqty));
            gettot= Integer.parseInt(tamt.getText());
            tamt.setText(String.valueOf(gettot-totrate));
            model.removeRow(itable.getSelectedRow());
             int count2 = itable.getSelectedColumnCount();
             icnt.setText(String.valueOf(count2-count));
             item1.setText("");
             tbqty.setText("");
        }

         }
         if(evt.getKeyCode() == KeyEvent.VK_DELETE){
                     DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty");
            }else{
                msg.setText("You must select a row");
            }
        }else{
           
            model.removeRow(itable.getSelectedRow());
            
        }

         }
    }//GEN-LAST:event_bdeleteKeyPressed

    private void csaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_csaveKeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                     
          DefaultTableModel model=(DefaultTableModel) itable.getModel();
                  try{  
          ResultSet rs,rs1;

                int cid = 0;
                statususe=0;
                String  cust_mob=mob.getText();
                Double Netamt=Double.parseDouble(netAmt.getText());
                System.out.println("net bill+netAmt.getText()"+Netamt);
                int bn=Integer.parseInt(bno.getText());
                System.out.println("bill no+bno.getText()"+bn);
                String dat=dt.getText();
                System.out.println("date =="+dat);
                double grandtot=Double.parseDouble(tamt.getText());
                System.out.println("tot =="+grandtot);
                double disc=Double.parseDouble(dis.getText());
                System.out.println("dis =="+disc);
                double paidamount=Double.parseDouble(paidAmt1.getText());
                System.out.println("p amt  =="+paidamount);
                double pendding=Double.parseDouble(pend1.getText());
                pendding=Double.parseDouble(new DecimalFormat("##.####").format(pendding));
                System.out.println("pend =="+pendding);
                double tax=Double.parseDouble(taxamt.getText());
                System.out.println("dis =="+tax);
                System.out.println("tax %"+taxamt.getText());
                String cust_name=cname.getText();
                System.out.println("cust cname"+cust_name);
                System.out.println("hi");
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
               
                rs=st.executeQuery("select cid from customer_info_table where cust_name='"+cust_name+"'");
                while(rs.next()){
                     cid=Integer.parseInt(rs.getString(1));
                }
                System.out.println("cid"+cid);
                st.executeUpdate("insert into bill_table(bill_no,bill_date,grand_bill,discount,net_bill,paid_amt,pending_amt,status,cid,tax)values('"+bn+"','"+dat+"','"+grandtot+"','"+disc+"','"+Netamt+"','"+paidamount+"','"+pendding+"','"+statususe+"','"+cid+"','"+tax+"')");
            JOptionPane.showMessageDialog(panel,"Successfully Saved");  
             //   st.executeUpdate("insert into customer_info_table(cust_name,cust,mobile) values('"+cust_name+"','"+cust_mob+"')");
                
            //    st.executeUpdate("insert into sales (item_name,price,qty,total,item_date) values('"++"','"++"','"++"','"++"','"++"')");
            JOptionPane.showMessageDialog(panel,"Successfully Saved");

        }
        catch(Exception ex){
            System.out.println(ex);
        }

         }
    }//GEN-LAST:event_csaveKeyPressed

    private void mobKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_mobKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_mobKeyReleased

    private void mobKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_mobKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_TAB){
            item1.requestFocusInWindow();
        }
    }//GEN-LAST:event_mobKeyPressed

    private void itableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itableKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_DOWN ||evt.getKeyCode()==KeyEvent.VK_UP || evt.getKeyCode()==KeyEvent.VK_ENTER ){
        int qty;
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
        tbqty.setText(model.getValueAt(itable.getSelectedRow(),3).toString());
         rate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),2).toString());
        totrate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),4).toString());   ///sum of total column
       
    }
    }//GEN-LAST:event_itableKeyPressed

    private void list2ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_list2ValueChanged
        // TODO add your handling code here:
     //    String selected=list2.getSelectedValue().toString();
       //  cname.setText(selected);
    }//GEN-LAST:event_list2ValueChanged

    private void Net1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Net1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        System.out.println("Net amt"+totamt);
        System.out.println("cal"+taxAmt);
        System.out.println("caldis"+discAmt);
        double net_amt=(totamt+taxAmt-discAmt);
        System.out.println("Net amt"+net_amt);
        netAmt.setText(String.valueOf(net_amt));
         }
    }//GEN-LAST:event_Net1KeyPressed

    private void list2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list2KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        cname.setText(list2.getSelectedValue());
        flag=1;
        jScrollPane3.setVisible(false);
        mob.requestFocusInWindow();
        }
    }//GEN-LAST:event_list2KeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try{
            con = DBConnection.getDBConnection();                                          
            String name=cname.getText();
            int mobi=Integer.parseInt(mob.getText());
            PreparedStatement ps = con.prepareStatement("insert into customer_info_table(cust_name,cust_mobile)values('"+name+"','"+mobi+"')");
            System.out.println("New cust"+name+mobi);
            ps.executeUpdate();
            cname.setText("");
        mob.setText("");
            msg.setText("Customer Saved");
        }
        catch(Exception e){}

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            try{
                String name=cname.getText();
                int mobi=Integer.parseInt(mob.getText());
                con = DBConnection.getDBConnection();                                          //

                PreparedStatement ps = con.prepareStatement("insert into customer_info_table(cust_name,cust_mobile)values('"+name+"','"+mobi+"')");
                // ps.setString(1,);
                System.out.println("New cust"+name+mobi);
                ps.executeUpdate();
                  cname.setText("");
        mob.setText("");
            msg.setText("Cust Saved");
            }
            catch(Exception e){}

        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void itableKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itableKeyTyped
        // TODO add your handling code here:
        int qty;
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
        tbqty.setText(model.getValueAt(itable.getSelectedRow(),3).toString());
        
        rate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),2).toString());
        totrate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),4).toString());   ///sum of total column
       
    }//GEN-LAST:event_itableKeyTyped

    private void bupdateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bupdateKeyTyped
        // TODO add your handling code here:
                     msg.setText("");
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty.");
            }else{
                msg.setText("You must select a row.");
            }
        }
        else
        {
            int user=0;
              selectqty=Integer.parseInt(tbqty.getText());
          //  int qty=Integer.parseInt(tbqty.getText());
            String iname=item1.getText();
            
            model.setValueAt(item1.getText().toString(),itable.getSelectedRow(),1);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from stock where item_name='"+iname+"'");
                while(rst1.next())
                {
                    user=rst1.getInt(4);             //user saving value of price from db
                }
            }catch(Exception e){}
            model.setValueAt(user,itable.getSelectedRow(),2);
            int total=user*selectqty;
            model.setValueAt(tbqty.getText(),itable.getSelectedRow(),3);
        
            model.setValueAt(total,itable.getSelectedRow(),4);
            msg.setText("Data Updated Successfully");
            
              System.out.println(".rate of selected row item....."+rate);
            getQty = Integer.parseInt(qtyt.getText());
          getsum();
             item1.setText("");
             tbqty.setText("");
            
        }
    }//GEN-LAST:event_bupdateKeyTyped

    private void pend1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pend1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pend1ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void list1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_list1KeyTyped

    private void list1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            String selected=list1.getSelectedValue();
            item1.setText(selected);

            //  sto.setText(String.valueOf(stock));
            flag=1;
            jScrollPane3.setVisible(false);
            tbqty.requestFocusInWindow();
            String str1="select qty from stock where item_name='"+item1.getText()+"'";

            try
            {
                con = DBConnection.getDBConnection();
                PreparedStatement pst=con.prepareStatement(str1);

                ResultSet rs=pst.executeQuery();
                while(rs.next())
                {

                    sto.setText(rs.getString(1));
                }

            }    catch(SQLException e)
            {
                System.out.println(e);
            }
        }
    }//GEN-LAST:event_list1KeyPressed

    private void list1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_list1PropertyChange
        // TODO add your handling code here
    }//GEN-LAST:event_list1PropertyChange

    private void list1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list1MouseClicked
        // TODO add your handling code here:
        String selected=list1.getSelectedValue();
        item1.setText(selected);

        //  sto.setText(String.valueOf(stock));
        flag=1;
        jScrollPane3.setVisible(false);
        tbqty.requestFocusInWindow();
        String str1="select qty from stock where item_name='"+item1.getText()+"'";

        try
        {
            con = DBConnection.getDBConnection();
            PreparedStatement pst=con.prepareStatement(str1);

            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {

                sto.setText(rs.getString(1));
            }

        }    catch(SQLException e)
        {
            System.out.println(e);
        }

    }//GEN-LAST:event_list1MouseClicked

    private void item1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_item1KeyReleased

        DefaultListModel l=new DefaultListModel();

        DefaultListModel listmodel=new DefaultListModel();
        jScrollPane3.setVisible(true);

        String str;
        String str1="select item_name,qty from stock where item_name LIKE ?";

        str=item1.getText();
        try
        {
            con = DBConnection.getDBConnection();
            PreparedStatement pst=con.prepareStatement(str1);
            pst.setString(1,str+"%");
            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
                listmodel.addElement(rs.getString(1));
                sto.setText(rs.getString(2));
                stock=Integer.parseInt(rs.getString(2));
            }
            list1.setModel(listmodel);
            // list1.setSelectedIndex(0);
            if(listmodel.isEmpty())
            {

            }
            else
            list1.setSelectedValue(listmodel.lastElement(), true);

        }
        catch(SQLException e)
        {
            System.out.println(e);
        }

    }//GEN-LAST:event_item1KeyReleased

    private void item1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_item1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){

        }
    }//GEN-LAST:event_item1KeyPressed

    private void item1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_item1ActionPerformed
        // TODO add your handling code here:
        item1.setText(list1.getSelectedValue());
        flag=1;
        jScrollPane3.setVisible(false);
        tbqty.requestFocusInWindow();

    }//GEN-LAST:event_item1ActionPerformed

    private void item1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_item1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_item1MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void statActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_statActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(finalbilingl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(finalbilingl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(finalbilingl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(finalbilingl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new finalbilingl().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Net1;
    private javax.swing.JButton badd;
    private javax.swing.JButton bdelete;
    private javax.swing.JLabel bno;
    private javax.swing.JButton bupdate;
    private javax.swing.JTextField cname;
    private javax.swing.JButton csave;
    private javax.swing.JTextField dAmt;
    private javax.swing.JTextField dis;
    private javax.swing.JLabel dt;
    private javax.swing.JLabel dt1;
    private javax.swing.JTextField icnt;
    private javax.swing.JTable itable;
    private javax.swing.JTextField item1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JList<String> list1;
    private javax.swing.JList<String> list2;
    private javax.swing.JTextField mob;
    private javax.swing.JTextField msg;
    private javax.swing.JTextField netAmt;
    private javax.swing.JPanel netb;
    private javax.swing.JTextField paidAmt1;
    private javax.swing.JTextField pend1;
    private javax.swing.JTextField qtyt;
    private javax.swing.JTextField rbno;
    private javax.swing.JComboBox<String> stat;
    private javax.swing.JLabel sto;
    private javax.swing.JTextField tamt;
    private javax.swing.JTextField taxamt;
    private javax.swing.JTextField taxinclud;
    private javax.swing.JTextField tbqty;
    // End of variables declaration//GEN-END:variables
}
