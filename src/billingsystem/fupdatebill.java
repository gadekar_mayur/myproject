
package billingsystem;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class fupdatebill extends javax.swing.JFrame {
     String date1;
      int bill_no=0;
      int count = 0;
      int getTax =0;
      int tqty;
      double totamt=0;
       double discAmt=0;
       double taxAmt=0;
       double cal;
       double caldis;
       int selectqty;
       int getQty;
       double totrate, gettot,sum;
       int paid_amt;
       int pend_amt;
       int NetAmt;
       double rate;
      private int id_no;
    private Connection con;
     String date;
     int flag=1;
    private String status;
    int disAmt;
   private String time1;
    private String time2;
    String name;
    int quantity;
    double SerchAmt;
   // private String user;
    //DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
    public fupdatebill() {
        initComponents();
         this.pack();
        this.setLocationRelativeTo(null);
       DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
       date=dateformat.format(new Date());
       dt.setText(date);
        Calendar cal = Calendar.getInstance();
       SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        time1=sdf1.format(cal.getTime());
        System.out.println(time1);
       dt2.setText(String.valueOf(sdf1.format(cal.getTime())));
       totamt=0.0;taxAmt=0.0;discAmt=0.0;
        //int bn=Integer.parseInt(bnum.getText());
        bnum.requestFocusInWindow();
        int b_num;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        bnum = new javax.swing.JTextField();
        dt = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dt2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        itable = new javax.swing.JTable();
        netb = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        icnt = new javax.swing.JTextField();
        tamt = new javax.swing.JTextField();
        taxamt = new javax.swing.JTextField();
        dis = new javax.swing.JTextField();
        rbno = new javax.swing.JTextField();
        bamt = new javax.swing.JTextField();
        dAmt = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        taxinclud = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        qtyt = new javax.swing.JTextField();
        Net2 = new javax.swing.JButton();
        netAmt = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        pend1 = new javax.swing.JTextField();
        paidAmt1 = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        stat = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        ret = new javax.swing.JTextField();
        bupdate = new javax.swing.JButton();
        bdelete = new javax.swing.JButton();
        msg = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        sto = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        tbqty = new javax.swing.JTextField();
        csave = new javax.swing.JButton();
        item1 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        list1 = new javax.swing.JList<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMaximumSize(new java.awt.Dimension(768, 541));
        jPanel1.setMinimumSize(new java.awt.Dimension(768, 541));
        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.blue, java.awt.Color.red, java.awt.Color.blue));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel1.setText("Enter Bill No.");

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/serch.png"))); // NOI18N
        jButton1.setMnemonic('s');
        jButton1.setText("Search");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.setIconTextGap(10);
        jButton1.setNextFocusableComponent(item1);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        bnum.setNextFocusableComponent(item1);
        bnum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnumActionPerformed(evt);
            }
        });

        dt.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        dt.setText("jLabel18");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel4.setText("Date/Time");

        dt2.setText("jLabel2");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(bnum, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dt2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dt)
                            .addComponent(dt2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(bnum)
                            .addGap(9, 9, 9)))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 60, 490, 70);

        itable.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.cyan, java.awt.Color.darkGray, java.awt.Color.cyan, java.awt.Color.black));
        itable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Item Name", "Rate", "Qty", "Total"
            }
        ));
        itable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                itableMouseClicked(evt);
            }
        });
        itable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                itableKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                itableKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(itable);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(10, 260, 480, 200);

        netb.setBackground(new java.awt.Color(255, 255, 255));
        netb.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.red, java.awt.Color.white, java.awt.Color.red, java.awt.Color.white));
        netb.setMinimumSize(new java.awt.Dimension(560, 700));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel8.setText("No. of Item");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel9.setText("Total Amount");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel13.setText("Tax(%)");

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel14.setText("Discount(%)");

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel15.setText("Return Bill No.");

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel16.setText("Return Bill Amt");

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel17.setText("Disc Amt");

        jLabel18.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel18.setText("Net Sales");

        icnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                icntActionPerformed(evt);
            }
        });

        taxamt.setText("0");
        taxamt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxamtActionPerformed(evt);
            }
        });
        taxamt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                taxamtKeyReleased(evt);
            }
        });

        dis.setText("0");
        dis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disActionPerformed(evt);
            }
        });
        dis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                disKeyReleased(evt);
            }
        });

        rbno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbnoActionPerformed(evt);
            }
        });

        bamt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bamtActionPerformed(evt);
            }
        });

        dAmt.setText("0");
        dAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dAmtActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel19.setText("Tax includes");

        taxinclud.setText("0");

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel20.setText("Total  Qty");

        qtyt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qtytActionPerformed(evt);
            }
        });

        Net2.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        Net2.setMnemonic('n');
        Net2.setText("Net  Bill ");
        Net2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Net2ActionPerformed(evt);
            }
        });
        Net2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Net2KeyPressed(evt);
            }
        });

        netAmt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                netAmtActionPerformed(evt);
            }
        });
        netAmt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                netAmtKeyReleased(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel36.setText("Paid Amount");

        jLabel37.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel37.setText("Pendding");

        paidAmt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paidAmt1ActionPerformed(evt);
            }
        });
        paidAmt1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                paidAmt1KeyReleased(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel38.setText("Status");

        stat.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        stat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pendding", "Paid" }));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel5.setText("Return Amt");

        javax.swing.GroupLayout netbLayout = new javax.swing.GroupLayout(netb);
        netb.setLayout(netbLayout);
        netbLayout.setHorizontalGroup(
            netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, netbLayout.createSequentialGroup()
                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, netbLayout.createSequentialGroup()
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(icnt)
                                    .addComponent(qtyt)
                                    .addComponent(tamt, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(taxamt, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(dAmt, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(netbLayout.createSequentialGroup()
                                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(rbno, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                                                .addComponent(dis, javax.swing.GroupLayout.Alignment.LEADING))
                                            .addComponent(bamt, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, netbLayout.createSequentialGroup()
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(13, 13, 13)
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(taxinclud)
                                    .addComponent(jTextField8))))
                        .addGap(44, 44, 44))
                    .addGroup(netbLayout.createSequentialGroup()
                        .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Net2, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                            .addComponent(netAmt)
                            .addGroup(netbLayout.createSequentialGroup()
                                .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                                .addComponent(pend1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(netbLayout.createSequentialGroup()
                                .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(stat, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(netbLayout.createSequentialGroup()
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(paidAmt1, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                                    .addComponent(ret))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        netbLayout.setVerticalGroup(
            netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netbLayout.createSequentialGroup()
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(icnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(qtyt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(taxamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(netbLayout.createSequentialGroup()
                        .addComponent(dis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rbno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(netbLayout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dAmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(taxinclud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Net2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(netAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(paidAmt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(ret, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel37)
                    .addComponent(pend1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(netbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(205, Short.MAX_VALUE))
        );

        jPanel1.add(netb);
        netb.setBounds(490, 0, 260, 520);

        bupdate.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bupdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/update.png"))); // NOI18N
        bupdate.setMnemonic('u');
        bupdate.setText("Update");
        bupdate.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bupdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bupdateActionPerformed(evt);
            }
        });
        bupdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bupdateKeyPressed(evt);
            }
        });
        jPanel1.add(bupdate);
        bupdate.setBounds(115, 490, 110, 30);

        bdelete.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bdelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/delete1.jpg"))); // NOI18N
        bdelete.setMnemonic('d');
        bdelete.setText("Delete");
        bdelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bdelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bdeleteActionPerformed(evt);
            }
        });
        bdelete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bdeleteKeyPressed(evt);
            }
        });
        jPanel1.add(bdelete);
        bdelete.setBounds(0, 490, 110, 29);

        msg.setForeground(new java.awt.Color(255, 0, 0));
        msg.setText("jLabel2");
        jPanel1.add(msg);
        msg.setBounds(40, 464, 390, 20);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText(" Item Name");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 130, 170, 30);

        jLabel21.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel21.setText("Available Stock");
        jPanel1.add(jLabel21);
        jLabel21.setBounds(240, 154, 100, 20);

        sto.setForeground(new java.awt.Color(255, 51, 51));
        sto.setText("NaN");
        jPanel1.add(sto);
        sto.setBounds(340, 150, 70, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Quantity");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(240, 194, 70, 20);

        tbqty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbqtyActionPerformed(evt);
            }
        });
        jPanel1.add(tbqty);
        tbqty.setBounds(340, 190, 110, 30);

        csave.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        csave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/print.jpg"))); // NOI18N
        csave.setMnemonic('b');
        csave.setText("Save and Bill");
        csave.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        csave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                csaveActionPerformed(evt);
            }
        });
        csave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                csaveKeyPressed(evt);
            }
        });
        jPanel1.add(csave);
        csave.setBounds(230, 490, 150, 31);

        item1.setNextFocusableComponent(list1);
        item1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                item1MouseClicked(evt);
            }
        });
        item1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                item1ActionPerformed(evt);
            }
        });
        item1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                item1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                item1KeyReleased(evt);
            }
        });
        jPanel1.add(item1);
        item1.setBounds(10, 160, 180, 30);

        list1.setNextFocusableComponent(tbqty);
        list1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list1MouseClicked(evt);
            }
        });
        list1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                list1PropertyChange(evt);
            }
        });
        list1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                list1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                list1KeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(list1);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(10, 190, 180, 70);

        jPanel3.setBackground(new java.awt.Color(255, 153, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.blue, java.awt.Color.blue, java.awt.Color.blue, java.awt.Color.lightGray));

        jLabel2.setFont(new java.awt.Font("Copperplate Gothic Bold", 1, 14)); // NOI18N
        jLabel2.setText("                  Update Bill ");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/billingsystem/Update.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 104, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 0, 490, 60);

        jButton2.setBackground(new java.awt.Color(255, 204, 204));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton2.setMnemonic('g');
        jButton2.setText("Go Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(385, 490, 100, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 758, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
            
            public int getsum(){
                   int rowsCount = itable.getRowCount();
                    sum = 0.0;
                    tqty=0;
                for(int i = 0; i < rowsCount; i++){
                                            sum = sum+Double.parseDouble(itable.getValueAt(i,4).toString());
                                             sum=Double.parseDouble(new DecimalFormat("##.####").format(sum));
                                            tqty=tqty+Integer.parseInt(itable.getValueAt(i,3).toString());
                                                    }
                                            tamt.setText(Double.toString((double) sum));
                                            qtyt.setText(Integer.toString(tqty));
                                            //return sum;
                                            System.out.println("sum"+sum);
     return tqty;
                            }
  public void updatetable()
       {
       try{
       con = DBConnection.getDBConnection();
       Statement st,st1,st2,st3,st4;
       ResultSet  rs,rs1,rs2,rs3,rs4,rs5;
       st4=con.createStatement();
       String bilno=bnum.getText();
       System.out.println("bill no add Table"+bilno);
         /* for(int i = 0; i < itable.getRowCount(); i++){
       
               String id=(itable.getValueAt(i,0).toString());
               int parseid=Integer.parseInt(id);
               String name=(itable.getValueAt(i,1).toString());
               String price=(itable.getValueAt(i,2).toString());
               String qty=(itable.getValueAt(i,3).toString());//after order taken(sales)2 - actual qty(stock)10
               int parseqty=Integer.parseInt(qty);
               System.out.println("parse user quantity ="+parseqty);
               String total=(itable.getValueAt(i,4).toString());
               //st.executeUpdate("insert into sales (cid,item_id,bno,item_name,price,qty,total) values('"+cid+"','"+id+"','"+bilno+"','"+name+"','"+price+"','"+qty+"','"+total+"')");
       }*/
       //  st4.executeUpdate("update sales set item_id='"+id_no+"',item_name='"+name+"',price='"+rate+"',qty='"+quantity+"',total='"+totrate+"' where item_id='"+id_no+"'");
              
                 st1=con.createStatement();
                rs1=st1.executeQuery("select * from sales where bno='"+bilno+"'");
               while(rs1.next())
               {
                   int item_id=rs1.getInt("item_id");
                   int order_qty=rs1.getInt("qty");
                   st2=con.createStatement();
                rs2=st2.executeQuery("select * from stock where idstock='"+item_id+"'");
               while(rs2.next())
               {
                   int stock_qty=rs2.getInt("qty");
                   int final_qty=stock_qty-order_qty;
                   st3=con.createStatement();
               st3.executeUpdate("Update stock set qty='"+final_qty+"' where idstock='"+item_id+"'");
               
               System.out.println("Stock updated successfully finalBiling ........");
               }
                   
               }
               
           System.out.println("End of for loop");
           }
         catch(SQLException e)
        {
            System.out.println("Inside InitCombobox");
            System.err.print(e);
            e.printStackTrace();
        } 
       
       }
     
            
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

             if(bnum.getText().equalsIgnoreCase(""))
        {
            JOptionPane.showMessageDialog (null, "Please Enter Bill Number ", "Data Fill", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            int b_num=Integer.parseInt(bnum.getText());
            int i=0,billamt=0,totbill=0;
            String date1,date2;
            //SerchAmt=Double.parseDouble(netAmt.getText());
            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            model.setRowCount(0);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
               
                ResultSet rs2=st.executeQuery("select * from sales where bno ='"+b_num+"'");
                while(rs2.next())
                {
              model.addRow(new Object[]{rs2.getInt(3),rs2.getString(5),rs2.getDouble(6),rs2.getString(7),rs2.getDouble(8)});
                   count = itable.getRowCount();
                }
                icnt.setText(String.valueOf(count));
                getsum();
                  ResultSet rs3=st.executeQuery("select * from bill_table where bill_no ='"+b_num+"'");
                  while(rs3.next())
                  {
                      taxAmt=rs3.getInt(7);
                    disAmt = rs3.getInt(6);
                     NetAmt=rs3.getInt(8);
                     paid_amt=rs3.getInt(9);
                     pend_amt=rs3.getInt(10);
                     
                     status=rs3.getString(11);
                  }
                  taxamt.setText(String.valueOf(taxAmt));
                 // taxinclud.setText();
                 dis.setText(String.valueOf(disAmt));
                 netAmt.setText(String.valueOf(NetAmt));
                 paidAmt1.setText(String.valueOf(paid_amt));
                 pend_amt=Integer.parseInt(new DecimalFormat("##.####").format(pend_amt));
                 pend1.setText(String.valueOf(pend_amt));
                 stat.setToolTipText(status);
                 SerchAmt=Double.parseDouble(netAmt.getText());
               }

            catch(SQLException e)
            {
                System.out.println("Inside InitCombobox");
                System.err.print(e);
            }
        
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void itableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_itableMouseClicked
        int qty;
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
       tbqty.setText(model.getValueAt(itable.getSelectedRow(),3).toString());
       quantity=Integer.parseInt((model.getValueAt(itable.getSelectedRow(),3).toString()));
        name=(model.getValueAt(itable.getSelectedRow(),1).toString());

         rate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),2).toString());
        totrate=Double.parseDouble(model.getValueAt(itable.getSelectedRow(),4).toString());   ///sum of total column
       
    }//GEN-LAST:event_itableMouseClicked

    private void itableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itableKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_DOWN ||evt.getKeyCode()==KeyEvent.VK_UP ){
            
            int qty;
            int row=itable.getSelectedRow();
            String t_click=(itable.getModel().getValueAt(row,0).toString());
            
            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
            int  rate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),2).toString());
            totrate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),3).toString());   ///sum of total column
             id_no=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),0).toString());
             //item1.setVisible(false);
        }
    }//GEN-LAST:event_itableKeyPressed

    private void icntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_icntActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_icntActionPerformed

    private void taxamtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxamtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_taxamtActionPerformed

    private void taxamtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxamtKeyReleased
        // TODO add your handling code here:

        totamt=Integer.parseInt(tamt.getText());
        taxAmt= (Double.parseDouble(taxamt.getText())/100)*totamt;
         taxAmt=Double.parseDouble(new DecimalFormat("##.####").format(taxAmt));
        System.out.println("tax%="+taxAmt);
        cal=totamt-taxAmt;
        taxinclud.setText(String.valueOf(taxAmt));

    }//GEN-LAST:event_taxamtKeyReleased

    private void disActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_disActionPerformed

    private void disKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_disKeyReleased
        // TODO add your handling code here:

        totamt=Integer.parseInt(tamt.getText());
        discAmt= (Double.parseDouble(dis.getText())/100)*totamt;
        discAmt=Double.parseDouble(new DecimalFormat("##.####").format(discAmt));
        caldis=totamt-discAmt;
        dAmt.setText(String.valueOf(discAmt));
    }//GEN-LAST:event_disKeyReleased

    private void rbnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbnoActionPerformed

    private void dAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dAmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dAmtActionPerformed

    private void Net2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Net2ActionPerformed
        // TODO add your handling code here:
        taxAmt= (Double.parseDouble(taxinclud.getText()));
        totamt= (Double.parseDouble(tamt.getText()));    
        discAmt=(Double.parseDouble(dAmt.getText()));    
        System.out.println("total amt"+totamt);
        System.out.println("cal"+taxAmt);
        System.out.println("caldis"+discAmt);
        double net_amt=(totamt+taxAmt-discAmt);
        double val = Math.round(net_amt);
        System.out.println("Net amt"+net_amt);
        netAmt.setText(String.valueOf(val));
        jTextField8.setText(String.valueOf(net_amt));
        
        double diff=val-SerchAmt;
        ret.setText(String.valueOf(diff));
    }//GEN-LAST:event_Net2ActionPerformed

    private void Net2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Net2KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            System.out.println("Net amt"+totamt);
            System.out.println("cal"+taxAmt);
            System.out.println("caldis"+discAmt);
            double net_amt=(totamt+taxAmt-discAmt);
            System.out.println("Net amt"+net_amt);
            netAmt.setText(String.valueOf(net_amt));
        }
    }//GEN-LAST:event_Net2KeyPressed

    private void netAmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_netAmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_netAmtActionPerformed

    private void netAmtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_netAmtKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_netAmtKeyReleased

    private void paidAmt1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_paidAmt1KeyReleased
        // TODO add your handling code here:
        double getNet=(Double.parseDouble(netAmt.getText()));
        System.out.println("getNet"+getNet);
        double getPaid=(Double.parseDouble(paidAmt1.getText()));
        System.out.println("Get Paid amt"+getPaid);
        System.out.println(getPaid);
        double calPend=getNet-getPaid;
        pend1.setText(String.valueOf(calPend));
        if(pend1.getText().equals("0.0")){
            stat.setSelectedIndex(1);
            status="Paid";
        }
        else{
            stat.setSelectedIndex(0);
            status="Pendding";

        }
    }//GEN-LAST:event_paidAmt1KeyReleased

    private void qtytActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qtytActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_qtytActionPerformed

    private void bupdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bupdateActionPerformed
        DefaultTableModel model1=(DefaultTableModel) itable.getModel();
        int id_num=Integer.parseInt(model1.getValueAt(itable.getSelectedRow(),0).toString());
        int iid = 0;
        msg.setText("");
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty.");
            }else{
                msg.setText("You must select a row.");
            }
        }else
        {
            double user=0.0;
            selectqty=Integer.parseInt(tbqty.getText());
            String iname=item1.getText();
            model.setValueAt(item1.getText().toString(),itable.getSelectedRow(),1);
            try{
                 int bn=Integer.parseInt(bnum.getText());
                 System.out.println("In Update Btn  bill bno="+bn);
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from stock where item_name='"+iname+"'");
                while(rst1.next())
                {
                    user=rst1.getDouble(4);             //user saving value of price from db
                    iid=rst1.getInt(1);
                }
            model.setValueAt(iid,itable.getSelectedRow(),0);  
            System.out.println("Updated id number="+iid);
            model.setValueAt(iname,itable.getSelectedRow(),1);
            System.out.println("Updated id name="+iname);
            model.setValueAt(user,itable.getSelectedRow(),2);
            double total=user*selectqty;
            System.out.println("Updated id price="+user);
            model.setValueAt(tbqty.getText(),itable.getSelectedRow(),3);
            model.setValueAt(total,itable.getSelectedRow(),4);
            msg.setText("Data Updated Successfully");
            System.out.println("Updated total="+total);
            System.out.println("Item ID of selected row....."+id_num);
            getQty = Integer.parseInt(tbqty.getText());
            System.out.println("Updated Quantiy="+getQty);
            getsum();
            item1.setText("");        tbqty.setText("");
            Statement st1=con.createStatement();
            st1.executeUpdate("update sales set item_id='"+iid+"',item_name='"+iname+"',price='"+user+"',qty='"+getQty+"',total='"+total+"'"
                    + " where item_id='"+id_num+"'");
            
            /*//UPDATE Stock
                 Statement stmt1 = null,stmt2,stmt3;
                st1 = con.createStatement();
              ResultSet rs1,rs2;
             int b_num=Integer.parseInt(bnum.getText());
               rs1=stmt1.executeQuery("select * from sales where bno='"+b_num+"'");
               while(rs1.next())
               {
                   int item_id=rs1.getInt("item_id");
                   int order_qty=rs1.getInt("qty");
                 
                   stmt2=con.createStatement();
               rs2=stmt2.executeQuery("select * from stock where idstock='"+item_id+"'");
               while(rs2.next())
               {
                   int stock_qty=rs2.getInt("qty");
                   int final_qty=stock_qty-order_qty;
                   stmt3=con.createStatement();
               stmt3.executeUpdate("Update stock set qty='"+final_qty+"' where idstock='"+item_id+"'");
               
               System.out.println("Stock updated successfully finalBiling 894");
               }
            }
             */      
                }
            catch(Exception e){
            e.printStackTrace();
            }
            }
    }//GEN-LAST:event_bupdateActionPerformed

    private void bupdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bupdateKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            msg.setText("");
            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            if(itable.getSelectedRow()==-1){
                if(itable.getSelectedRowCount()==0){
                    msg.setText("Table is empty.");
                }else{
                    msg.setText("You must select a row.");
                }
            }
            else
            {
                int user=0;
                selectqty=Integer.parseInt(tbqty.getText());
                 int qty=Integer.parseInt(tbqty.getText());
                String iname=item1.getText();

                model.setValueAt(item1.getText().toString(),itable.getSelectedRow(),1);
                try{
                    con = DBConnection.getDBConnection();
                    Statement st=con.createStatement();
                    ResultSet rst1=st.executeQuery("select * from stock where item_name='"+iname+"'");
                   while(rst1.next())
                    {
                        user=rst1.getInt(4);             //user saving price from db
                    }
                 
                }catch(Exception e){}
                model.setValueAt(user,itable.getSelectedRow(),2);
                int total=user*selectqty;
                model.setValueAt(tbqty.getText(),itable.getSelectedRow(),3);

                model.setValueAt(total,itable.getSelectedRow(),4);
                msg.setText("Data Updated Successfully");

               System.out.println(".rate of selected row item....."+rate);
                getQty = Integer.parseInt(qtyt.getText());
            getsum();
            item1.setText("");
                tbqty.setText("");

            }
        }
    }//GEN-LAST:event_bupdateKeyPressed

    private void bdeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bdeleteActionPerformed
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        if(itable.getSelectedRow()==-1){
            if(itable.getSelectedRowCount()==0){
                msg.setText("Table is empty");
            }else{
                msg.setText("You must select a row");
            }
        }else{
            getQty = Integer.parseInt(qtyt.getText());
            System.out.println("tbqty.getText()"+getQty);
            qtyt.setText(String.valueOf(getQty-selectqty));
            gettot= Integer.parseInt(tamt.getText());
            tamt.setText(String.valueOf(gettot-totrate));
         //   model.removeRow(itable.getSelectedRow());
             try{
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            st.executeUpdate("delete from sales where item_id='"+id_no+"'");
            System.out.println("Stoock it id_no="+id_no);
            JOptionPane.showMessageDialog (null," Item Deleted Successfully", "Item Deleted", JOptionPane.INFORMATION_MESSAGE);
            model.removeRow(itable.getSelectedRow());
             
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
            int count2 = itable.getSelectedColumnCount();
            icnt.setText(String.valueOf(count-count2));
        }
    }//GEN-LAST:event_bdeleteActionPerformed

    private void bdeleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bdeleteKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_DELETE){
            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            if(itable.getSelectedRow()==-1){
                if(itable.getSelectedRowCount()==0){
                    msg.setText("Table is empty");
                }else{
                    msg.setText("You must select a row");
                }
            }else{
                System.out.println("......"+rate);
                System.out.println(item1.getText());
                selectqty=Integer.parseInt(tbqty.getText());
                getQty = Integer.parseInt(qtyt.getText());
                System.out.println("tbqty.getText()"+getQty);
                qtyt.setText(String.valueOf(getQty-selectqty));
                gettot= Integer.parseInt(tamt.getText());
                tamt.setText(String.valueOf(gettot-totrate));
                model.removeRow(itable.getSelectedRow());
                try{
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
            st.executeUpdate("delete from sales where idstock='"+id_no+"'");
            System.out.println("Stoock it id_no="+id_no);
            JOptionPane.showMessageDialog (null," Item Deleted Successfully", "Item Deleted", JOptionPane.INFORMATION_MESSAGE);
            model.removeRow(itable.getSelectedRow());
             
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
item1.setText("");
                tbqty.setText("");
            }

        }
        if(evt.getKeyCode() == KeyEvent.VK_DELETE){
            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            if(itable.getSelectedRow()==-1){
                if(itable.getSelectedRowCount()==0){
                    msg.setText("Table is empty");
                }else{
                    msg.setText("You must select a row");
                }
            }else{

                model.removeRow(itable.getSelectedRow());

            }

        }
    }//GEN-LAST:event_bdeleteKeyPressed

    private void tbqtyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbqtyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbqtyActionPerformed

    private void bamtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bamtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bamtActionPerformed

    private void itableKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_itableKeyTyped
        // TODO add your handling code here:
        int qty;
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        item1.setText(model.getValueAt(itable.getSelectedRow(),1).toString());
       tbqty.setText(model.getValueAt(itable.getSelectedRow(),3).toString());

       rate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),2).toString());
        totrate=Integer.parseInt(model.getValueAt(itable.getSelectedRow(),4).toString());   ///sum of total column

    }//GEN-LAST:event_itableKeyTyped

    private void csaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_csaveActionPerformed
        DefaultTableModel model=(DefaultTableModel) itable.getModel();
        try{
            ResultSet rs,rs1;

          //  String  cust_mob=mob.getText();
            Double Netamt=Double.parseDouble(netAmt.getText());
            System.out.println("net bill+netAmt.getText()"+Netamt);
            int bn=Integer.parseInt(bnum.getText());
            System.out.println("bill no+bno.getText()"+bn);
            String dat=dt.getText();
            System.out.println("date+dt.getText()"+dat);
            double grandtot=Double.parseDouble(tamt.getText());
            System.out.println("tot amt+tamt.getText()"+grandtot);
            double disc=Double.parseDouble(dis.getText());
            System.out.println("dis %+dis.getText()"+disc);
            double paidamount=Double.parseDouble(paidAmt1.getText());
            System.out.println("p amt+paidAmt1.getText()"+paidamount);
            double pendding=Double.parseDouble(pend1.getText());
            System.out.println("pend+pend1.getText()"+pendding);
            double tax=Double.parseDouble(taxamt.getText());
            System.out.println("dis %+dis.getText()"+tax);
            System.out.println("tax %"+taxamt.getText());
            //String cust_name=cname.getText();
            //System.out.println("cust cname"+cust_name);
            System.out.println("hiiiiiiiiiiii");
            con = DBConnection.getDBConnection();
            Statement st=con.createStatement();
           // addtable();
            //rs=st.executeQuery("select cid from customer_info_table where cust_name='"+cust_name+"'");
           /// while(rs.next()){
              //  cid=rs.getInt(1);
                //System.out.println("cid inside while loop"+cid);
            //}
            //System.out.println("cid"+cid);
            st.executeUpdate("update bill_table set grand_bill='"+grandtot+"',discount='"+disc+"',net_bill='"+Netamt+"',paid_amt='"+paidamount+"',pending_amt='"+pendding+"',status='"+status+"',tax='"+tax+"' where bill_no='"+bn+"'");
           updatetable();
            //values('"+bn+"','"+dat+"','"+grandtot+"','"+disc+"','"+Netamt+"','"+paidamount+"','"+pendding+"','"+status+"','"+cid+"','"+tax+"')");
            /*   rs= st.executeQuery("select bno from sales where cid='"+cid+"'");
            while(rs.next())
            {
                bill_no=rs.getInt(1);
                System.out.println("bill no"+bill_no);
            }
            */

         //   bill_no=Integer.parseInt(bno.getText());
            System.out.println("This Final one bill no  ="+bill_no);
            //  FinalInvoice i=new  FinalInvoice(cid,bill_no);
          //  receipt r=new receipt(cid,bill_no);
          //  new receipt().setVisible(true);
            JOptionPane.showMessageDialog(null,"Successfully Saved");
              new FinalInvoic().setVisible(true);
            //   new FinalInvoice().setVisible(true);
            //st.executeUpdate("insert into bill_table(cid,bill_no,bill_date,grand_bill,discount,tax,net_bill,paid_amt,pending_amt,status)values('"+cid+"','"+bn+"','"+dat+"','"+grandtot+"','"+disc+"','"+tax+"','"+Netamt+"','"+paidamount+"','"+pendding+"','"+status+"')");
            //this.dispose();
            //  new createInvoice().setVisible(true);
        }
        catch(Exception ex){
            System.out.println(ex);
        }

    }//GEN-LAST:event_csaveActionPerformed

    private void csaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_csaveKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){

            DefaultTableModel model=(DefaultTableModel) itable.getModel();
            try{
                ResultSet rs,rs1;

                int cid = 0;
         //       statususe=0;
           //   String  cust_mob=mob.getText();
               
           Double Netamt=Double.parseDouble(netAmt.getText());
            double value = Math.round(Netamt * 100.0) / 100.0;
           System.out.println("net bill+netAmt.getText()"+Netamt);
                int bn=Integer.parseInt(bnum.getText());
                System.out.println("bill no+bno.getText()"+bn);
                String dat=dt.getText();
                System.out.println("date+dt.getText()"+dat);
                double grandtot=Double.parseDouble(tamt.getText());
                System.out.println("tot amt+tamt.getText()"+grandtot);
                double disc=Double.parseDouble(dis.getText());
                System.out.println("dis %+dis.getText()"+disc);
                double paidamount=Double.parseDouble(paidAmt1.getText());
                System.out.println("p amt+paidAmt1.getText()"+paidamount);
                double pendding=Double.parseDouble(pend1.getText());
                System.out.println("pend+pend1.getText()"+pendding);
                double tax=Double.parseDouble(taxamt.getText());
                System.out.println("dis %+dis.getText()"+tax);
                System.out.println("tax %"+taxamt.getText());
          //      String cust_name=cname.getText();
         //       System.out.println("cust cname"+cust_name);
                System.out.println("hi");
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();

         //       rs=st.executeQuery("select cid from customer_info_table where cust_name='"+cust_name+"'");
           //     while(rs.next()){
            //        cid=Integer.parseInt(rs.getString(1));
             //   }
                System.out.println("cid"+cid);
              st.executeUpdate("insert into bill_table(bill_no,bill_date,grand_bill,discount,net_bill,paid_amt,pending_amt,status,cid,tax)values('"+bn+"','"+dat+"','"+grandtot+"','"+disc+"','"+value+"','"+paidamount+"','"+pendding+"','"+status+"','"+cid+"','"+tax+"')");
                JOptionPane.showMessageDialog(null,"Successfully Saved");
                //   st.executeUpdate("insert into customer_info_table(cust_name,cust,mobile) values('"+cust_name+"','"+cust_mob+"')");

                //    st.executeUpdate("insert into sales (item_name,price,qty,total,item_date) values('"++"','"++"','"++"','"++"','"++"')");
       //         JOptionPane.showMessageDialog(null,"Successfully Saved");

            }
            catch(Exception ex){
                System.out.println(ex);
            }

        }
    }//GEN-LAST:event_csaveKeyPressed

    private void item1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_item1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_item1MouseClicked

    private void item1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_item1ActionPerformed
        // TODO add your handling code here:
        item1.setText(list1.getSelectedValue());
        flag=1;
        jScrollPane3.setVisible(false);
        tbqty.requestFocusInWindow();

    }//GEN-LAST:event_item1ActionPerformed

    private void item1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_item1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){

        }
    }//GEN-LAST:event_item1KeyPressed

    private void item1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_item1KeyReleased

        DefaultListModel l=new DefaultListModel();

        DefaultListModel listmodel=new DefaultListModel();
        jScrollPane3.setVisible(true);

        String str;
        String str1="select item_name,qty from stock where item_name LIKE ?";

        str=item1.getText();
        try
        {
            con = DBConnection.getDBConnection();
            PreparedStatement pst=con.prepareStatement(str1);
            pst.setString(1,str+"%");
            ResultSet rs=pst.executeQuery();
            while(rs.next())
            {
                listmodel.addElement(rs.getString(1));
                sto.setText(rs.getString(2));
             //   stock=Integer.parseInt(rs.getString(2));
            }
            list1.setModel(listmodel);
            // list1.setSelectedIndex(0);
            if(listmodel.isEmpty())
            {

            }
            else
            list1.setSelectedValue(listmodel.lastElement(), true);

        }
        catch(SQLException e)
        {
            System.out.println(e);
        }

    }//GEN-LAST:event_item1KeyReleased

    private void list1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list1MouseClicked
        // TODO add your handling code here:
        String selected=list1.getSelectedValue();
        item1.setText(selected);

      //  sto.setText(String.valueOf(stock));
        flag=1;
        jScrollPane3.setVisible(false);
        tbqty.requestFocusInWindow();
          String str1="select qty from stock where item_name='"+item1.getText()+"'";

            try
            {
                con = DBConnection.getDBConnection();
                PreparedStatement pst=con.prepareStatement(str1);
            
                ResultSet rs=pst.executeQuery();
                while(rs.next())
                {
                    
                    sto.setText(rs.getString(1));
                }
                
            }    catch(SQLException e)
            {
                System.out.println(e);
            }

    }//GEN-LAST:event_list1MouseClicked

    private void list1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_list1PropertyChange
        // TODO add your handling code here
    }//GEN-LAST:event_list1PropertyChange

    private void list1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            item1.setText(list1.getSelectedValue());
            flag=1;
            jScrollPane3.setVisible(false);
            tbqty.requestFocusInWindow();
              String str1="select qty from stock where item_name='"+item1.getText()+"'";

            try
            {
                con = DBConnection.getDBConnection();
                PreparedStatement pst=con.prepareStatement(str1);
            
                ResultSet rs=pst.executeQuery();
                while(rs.next())
                {
                    
                    sto.setText(rs.getString(1));
                  //  stock=Integer.parseInt(rs.getString(1));
                }
                
            }    catch(SQLException e)
            {
                System.out.println(e);
            }
        }
    }//GEN-LAST:event_list1KeyPressed

    private void list1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_list1KeyTyped

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.dispose();
        new adminpanel().setVisible(true);
        this.setDefaultCloseOperation(adminpanel.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void paidAmt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paidAmt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_paidAmt1ActionPerformed

    private void bnumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnumActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bnumActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(fupdatebill.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(fupdatebill.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(fupdatebill.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(fupdatebill.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new fupdatebill().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Net2;
    private javax.swing.JTextField bamt;
    private javax.swing.JButton bdelete;
    private javax.swing.JTextField bnum;
    private javax.swing.JButton bupdate;
    private javax.swing.JButton csave;
    private javax.swing.JTextField dAmt;
    private javax.swing.JTextField dis;
    private javax.swing.JLabel dt;
    private javax.swing.JLabel dt2;
    private javax.swing.JTextField icnt;
    private javax.swing.JTable itable;
    private javax.swing.JTextField item1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JList<String> list1;
    private javax.swing.JLabel msg;
    private javax.swing.JTextField netAmt;
    private javax.swing.JPanel netb;
    private javax.swing.JTextField paidAmt1;
    private javax.swing.JTextField pend1;
    private javax.swing.JTextField qtyt;
    private javax.swing.JTextField rbno;
    private javax.swing.JTextField ret;
    private javax.swing.JComboBox<String> stat;
    private javax.swing.JLabel sto;
    private javax.swing.JTextField tamt;
    private javax.swing.JTextField taxamt;
    private javax.swing.JTextField taxinclud;
    private javax.swing.JTextField tbqty;
    // End of variables declaration//GEN-END:variables
}
