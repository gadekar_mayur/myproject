/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package billingsystem;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.jdbc.JDBCCategoryDataset;

/**
 *
 * @author Shashank
 */
public class searchitem extends javax.swing.JFrame {

     private Connection con;
     String date;
    private String status;
    private String user;
    DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Creates new form searchitem
     */
    public searchitem() {
        initComponents();
        this.pack();
        this.setLocationRelativeTo(null);
        DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
        date=dateformat.format(new Date());
        dt.setText(date);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        dt = new javax.swing.JLabel();
        dt2 = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        dt1 = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton5 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        amt = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        amt1 = new javax.swing.JLabel();
        amt2 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        jButton4.setText("jButton4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 153, 204));

        jLabel1.setFont(new java.awt.Font("Copperplate Gothic Bold", 1, 18)); // NOI18N
        jLabel1.setText("Check Datewise Bill");

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/billingsystem/rcm-assesment-icon-200.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(56, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(0, 4, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 90));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel6.setText("Report Of :");

        dt.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        dt.setText("jLabel7");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("To Date:");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel8.setText("From Date :");

        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/serch.png"))); // NOI18N
        jButton1.setMnemonic('s');
        jButton1.setText("Search");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.setIconTextGap(10);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(dt, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(15, 15, 15)
                .addComponent(dt1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(dt2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dt, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dt1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dt2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 800, 100));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Bill No", "Customer Name", "Bill Amount", "Discount %", "Total Bill", "Paid Amount", "Pending Amount", "Status"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 800, 290));

        jButton5.setBackground(new java.awt.Color(255, 153, 153));
        jButton5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton5.setMnemonic('b');
        jButton5.setText("Show On Barchart");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jButton5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton5KeyPressed(evt);
            }
        });
        getContentPane().add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 490, -1, 30));

        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/apu/Images/print.jpg"))); // NOI18N
        jButton3.setMnemonic('p');
        jButton3.setText("Print");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jButton3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton3KeyPressed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 490, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Total");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        amt.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        amt.setText("jLabel5");
        getContentPane().add(amt, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Total");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        amt1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        amt1.setText("jLabel5");
        getContentPane().add(amt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        amt2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        amt2.setText("=");
        getContentPane().add(amt2, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 490, 80, 25));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel9.setText("Total");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 490, 100, 25));

        jButton2.setBackground(new java.awt.Color(255, 204, 204));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jButton2.setMnemonic('g');
        jButton2.setText("Go Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 490, 90, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
if(dt1.getDate()==null || dt2.getDate()==null)
        {
            JOptionPane.showMessageDialog (null, "Please Select Date ", "Data Fill", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            int i=0,billamt=0;
            double totbill=0;
            String date1,date2;
            date1=dateformat.format(dt1.getDate());
            date2=dateformat.format(dt2.getDate());
            DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from login_table where id='"+1+"'");
               while(rst1.next())
               {
                      user=rst1.getString(2);
               }
               ResultSet rs2=st.executeQuery("select bill_date,bill_no,cust_name,grand_bill,discount,net_bill,paid_amt,pending_amt,status from bill_table l1,customer_info_table l2 where l2.cid=l1.cid and l1.bill_date between '"+date1+"' and '"+date2+"'");
                
                while(rs2.next())
                {
                    billamt=billamt+Integer.parseInt(rs2.getString(4));
                    totbill=totbill+Double.parseDouble(rs2.getString(6));
                    if(rs2.getString(9).equalsIgnoreCase("0"))
                    {
                        status="Pending";
                    }
                    if(rs2.getString(9).equalsIgnoreCase("1"))
                    {
                        status="Paid";
                    }
                    model.addRow(new Object[]{rs2.getString(1),rs2.getString(2),rs2.getString(3),rs2.getString(4),rs2.getString(5),rs2.getString(6),rs2.getString(7),rs2.getString(8),rs2.getString(9),status});
                }
                model.addRow(new Object[]{"----------------------------------","--------------------------------","-------------------------------------","----------------------------------","--------------------------------","-------------------------------------"});
                model.addRow(new Object[]{"Total","","",billamt,"bill amt=",totbill});
                amt2.setText(String.valueOf(billamt));
            }

            catch(SQLException e)
            {
                System.out.println("Inside InitCombobox");
                System.err.print(e);
            }
        }        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

PrinterJob job = PrinterJob.getPrinterJob();
   MessageFormat[] header = new MessageFormat[4];
   header[0] = new MessageFormat("General Stroes");
   header[1] = new MessageFormat("ITEM WISE SALE REPORT");
   header[2] = new MessageFormat("Report Of:"+dateformat.format(new Date()));
   header[3] = new MessageFormat("Date:"+dateformat.format(dt1.getDate())+" To "+dateformat.format(dt2.getDate()));

  MessageFormat[] footer = new MessageFormat[3];
  footer[0] = new MessageFormat("  Submitted By");
  footer[1] = new MessageFormat("('"+user+"')");
  footer[2] = new MessageFormat("Page{0,number,integer}");
   job.setPrintable(new MyTablePrintable(jTable1, JTable.PrintMode.FIT_WIDTH, header, footer));

    if (job.printDialog())
      try {
        System.out.println("Calling PrintJob.print()");
        job.print();
        System.out.println("End PrintJob.print()");
      }
      catch (PrinterException pe) {
        System.out.println("Error printing: " + pe);
      }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        
String dat1,dat2;
        DateFormat dateformat=new SimpleDateFormat("yyyy-MM-dd");
        dat1=dateformat.format(dt1.getDate());
        dat2=dateformat.format(dt2.getDate());
        String query="SELECT bill_date,net_bill from bill_table where bill_date between '"+dat1+"' and '"+dat2+"'";
        try{
            con = DBConnection.getDBConnection();
            JDBCCategoryDataset dataset=new JDBCCategoryDataset(con,query);
            JFreeChart chart=ChartFactory.createBarChart("Sale Report", "Date", "Bill", dataset, PlotOrientation.VERTICAL, false, true, false);
            CategoryPlot p=chart.getCategoryPlot();
            p.setRangeGridlinePaint(Color.black);
            ChartFrame frame=new ChartFrame("Bar Chart For Sale Report",chart);
            frame.setVisible(true);
            frame.setSize(700,500);
            frame.setLocationRelativeTo(null);
        }
        catch(SQLException e)
        {
            System.out.println("Inside InitCombobox");
            System.err.print(e);
        }

// TODO add your handling code here:
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
         this.dispose();
        new adminpanel().setVisible(true);
        this.setDefaultCloseOperation(adminpanel.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton3KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
           
PrinterJob job = PrinterJob.getPrinterJob();
   MessageFormat[] header = new MessageFormat[4];
   header[0] = new MessageFormat("General Stroes");
   header[1] = new MessageFormat("ITEM WISE SALE REPORT");
   header[2] = new MessageFormat("Report Of:"+dateformat.format(new Date()));
   header[3] = new MessageFormat("Date:"+dateformat.format(dt1.getDate())+" To "+dateformat.format(dt2.getDate()));

  MessageFormat[] footer = new MessageFormat[3];
  footer[0] = new MessageFormat("  Submitted By");
  footer[1] = new MessageFormat("('"+user+"')");
  footer[2] = new MessageFormat("Page{0,number,integer}");
   job.setPrintable(new MyTablePrintable(jTable1, JTable.PrintMode.FIT_WIDTH, header, footer));

    if (job.printDialog())
      try {
        System.out.println("Calling PrintJob.print()");
        job.print();
        System.out.println("End PrintJob.print()");
      }
      catch (PrinterException pe) {
        System.out.println("Error printing: " + pe);
      }
 
        }
    }//GEN-LAST:event_jButton3KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
                    
if(dt1.getDate()==null || dt2.getDate()==null)
        {
            JOptionPane.showMessageDialog (null, "Please Select Date ", "Data Fill", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            int i=0,billamt=0;
            double totbill=0;
            String date1,date2;
            date1=dateformat.format(dt1.getDate());
            date2=dateformat.format(dt2.getDate());
            DefaultTableModel model=(DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            try{
                con = DBConnection.getDBConnection();
                Statement st=con.createStatement();
                ResultSet rst1=st.executeQuery("select * from login_table where id='"+1+"'");
               while(rst1.next())
               {
                      user=rst1.getString(2);
               }
               ResultSet rs2=st.executeQuery("select bill_date,bill_no,cust_name,grand_bill,discount,net_bill,paid_amt,pending_amt,status from bill_table l1,customer_info_table l2 where l2.cid=l1.cid and l1.bill_date between '"+date1+"' and '"+date2+"'");
                
                while(rs2.next())
                {
                    billamt=billamt+Integer.parseInt(rs2.getString(4));
                    totbill=totbill+Double.parseDouble(rs2.getString(6));
                    if(rs2.getString(9).equalsIgnoreCase("0"))
                    {
                        status="Pending";
                    }
                    if(rs2.getString(9).equalsIgnoreCase("1"))
                    {
                        status="Paid";
                    }
                    model.addRow(new Object[]{rs2.getString(1),rs2.getString(2),rs2.getString(3),rs2.getString(4),rs2.getString(5),rs2.getString(6),rs2.getString(7),rs2.getString(8),rs2.getString(9),status});
                }
                model.addRow(new Object[]{"----------------------------------","--------------------------------","-------------------------------------","----------------------------------","--------------------------------","-------------------------------------"});
                model.addRow(new Object[]{"Total","","",billamt,"bill amt=",totbill});
                amt2.setText(String.valueOf(billamt));
            }

            catch(SQLException e)
            {
                System.out.println("Inside InitCombobox");
                System.err.print(e);
            }
        }        

         }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton5KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            if(evt.getKeyCode()==KeyEvent.VK_ENTER){
           
PrinterJob job = PrinterJob.getPrinterJob();
   MessageFormat[] header = new MessageFormat[4];
   header[0] = new MessageFormat("General Stroes");
   header[1] = new MessageFormat("ITEM WISE SALE REPORT");
   header[2] = new MessageFormat("Report Of:"+dateformat.format(new Date()));
   header[3] = new MessageFormat("Date:"+dateformat.format(dt1.getDate())+" To "+dateformat.format(dt2.getDate()));

  MessageFormat[] footer = new MessageFormat[3];
  footer[0] = new MessageFormat("  Submitted By");
  footer[1] = new MessageFormat("('"+user+"')");
  footer[2] = new MessageFormat("Page{0,number,integer}");
   job.setPrintable(new MyTablePrintable(jTable1, JTable.PrintMode.FIT_WIDTH, header, footer));

    if (job.printDialog())
      try {
        System.out.println("Calling PrintJob.print()");
        job.print();
        System.out.println("End PrintJob.print()");
      }
      catch (PrinterException pe) {
        System.out.println("Error printing: " + pe);
      }
 
        }
        }
    }//GEN-LAST:event_jButton5KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(searchitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(searchitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(searchitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(searchitem.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new searchitem().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel amt;
    private javax.swing.JLabel amt1;
    private javax.swing.JLabel amt2;
    private javax.swing.JLabel dt;
    private com.toedter.calendar.JDateChooser dt1;
    private com.toedter.calendar.JDateChooser dt2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
